(function() {
    'use strict';

    angular.module('checklistApp', ['ui.tree', 'checklistService', 'ngMessages'], function($interpolateProvider) {
        $interpolateProvider.startSymbol('<%');
        $interpolateProvider.endSymbol('%>');
    })

    .controller('checklistCtrl', function($scope, $location, $http, Checklists, $timeout) {

        /**
         *
         * Add Task to Checklist
         *
         */
        $scope.addTasks = function() {
            var nodeData = $scope.data;
            var taskName = document.getElementById("taskName").value;
            if ((taskName.length > 0) && (taskName.length < 30)) {
                $scope.data.push({
                    id: nodeData.length + 1,
                    tasksID: null,
                    tasksname: $scope.newTasks,
                    editing: false,
                    note_editing: false,
                    tasksnote: null,
                    checked: false,
                    nodes: [],
                });
                $scope.newTasks = "";
            };
        };

        /**
         *
         * Add Notes to Checklist
         *
         */
        $scope.editNote = function(scope) {
            var nodeData = scope.$modelValue;
            nodeData.note_editing = true;
            nodeData.tasksnote = nodeData.tasksnote;
        };

        $scope.cancelNoteTask = function(scope) {
            var nodeData = scope.$modelValue;
            //var noteEdit = document.getElementById("noteEdit").value;
            if (nodeData.tasksnote != null) {
                if (nodeData.tasksnote.length < 30 && nodeData.tasksnote.length > 3) {
                    nodeData.note_editing = false;
                }
            };
            if (nodeData.tasksnote == null || nodeData.tasksnote.length == 0) {
                nodeData.note_editing = false;
                nodeData.tasksnote = null;
            };
        };

        $scope.editTask = function(scope) {
            var nodeData = scope.$modelValue;
            nodeData.editing = true;
            nodeData.tasksname = nodeData.tasksname;
        };

        $scope.cancelEditingTask = function(scope) {
            var nodeData = scope.$modelValue;
            if ((nodeData.tasksname.length > 0) && (nodeData.tasksname.length < 30)) {
                nodeData.editing = false;
            };
        };

        $scope.newSubItem = function(scope) {
            var nodeData = scope.$modelValue;
            nodeData.nodes.push({
                id: nodeData.id * 10 + nodeData.nodes.length,
                tasksname: nodeData.tasksname + '.' + (nodeData.nodes.length + 1),
                //tasksname: $scope.newTasks,
                editing: false,
                note_editing: false,
                tasksnote: null,
                checked: false,
                nodes: []
            });
        };

        $scope.filtershow = function(scope) {
            var nodeData = scope.$modelValue;
            if ((nodeData.note_editing == true) || (nodeData.editing == true)) {
                return false;
            } else {
                return true;
            };
        };

        $scope.itemClicked = function(scope) {
            var nodeData = scope.$modelValue;
            console.log(nodeData.checked);
            if (nodeData.checked == false) {
                nodeData.checked = true;
                console.log(nodeData.checked);
            } else {
                nodeData.checked = false;
                console.log("hallo");
            }
        }

        $scope.data = [];

        /**
         *
         *
         */
        $scope.addChecklistname = function() {
            $scope.data[0].checklistsname = $scope.checkNameModel;
            console.log($scope.data[0]);
        };

        $scope.addChecklistnote = function() {
            $scope.data[0].noteforchecklist = $scope.checkNoteModel;
            console.log($scope.data[0]);
        };

        /**
         *
         *
         *
         */
        $scope.loadChecklists = function(checklistsID) {
            Checklists.get(checklistsID)
                .success(function(data) {
                    console.log(data);
                    $scope.data = data;
                })
                .error(function(data) {
                    console.log(data);
                });
        }

        /**
         *
         * Function to save the Checklist and send it to Laravel Controller, with Service-Provider.
         *
         */
        $scope.saveChecklist = function(form) {
            // Trigger validation flag.
            $scope.submitted = true;

            var checkData = $scope.data;
            // save the Checklist, pass in Checklist data from in the parameter field
            // use the function we created in our service
            var checkName = document.getElementById("checklistsName").value;

            if ((checkData.length == 0)) {
                // alert("Checkliste/Titel ist leer!");
                $scope.messagestask = 'Bitte geben Sie mindestens einen Checkpunkt an.';
            }else {
                $scope.addChecklistname();
                //Checklists.sendChecklistsname(checkName);
                Checklists.save(checkData)
                    .success(function(data, status, headers, config) {
                        console.log(data);
                        $scope.submitted = false;
                        $scope.messages = 'Checkliste "' + checkName + '" wurde erfolgreich erstellt';
                        document.getElementById('checklistsSettings').removeAttribute("style");
                        document.getElementById('checklistsSettingsLink').removeAttribute("style");
                        document.getElementById('hiddenChecklistsname').value = checkName;
                    })
                    .error(function(data) {
                        console.log(data);
                    })
                $timeout(function() {
                    $scope.messages = null;
                }, 5000);
            }
        };

        /**
         *
         * Function to resave the Checklist and send it to Laravel Controller, with Service-Provider.
         *
         */
        $scope.resaveChecklist = function(form) {
            // Trigger validation flag.
            $scope.submitted = true;

            var checkData = $scope.data;
            // save the Checklist, pass in Checklist data from in the parameter field
            // use the function we created in our service
            var checkName = document.getElementById("checklistsName").value;

            if ((checkData.length == 0)) {
                // alert("Checkliste/Titel ist leer!");
                $scope.messagestask = 'Bitte geben Sie mindestens einen Checkpunkt an.';
            } else {
                $scope.addChecklistname();
                //Checklists.sendChecklistsname(checkName);
                Checklists.resave(checkData)
                    .success(function(data, status, headers, config) {
                        console.log(data);
                        $scope.submitted = false;
                        $scope.messages = 'Checkliste "' + checkName + '" wurde erfolgreich aktualisiert.';
                    })
                    .error(function(data) {
                        console.log(data);
                    })
                $timeout(function() {
                    $scope.messages = null;
                }, 5000);
            }
        };

        $scope.saveChecklistForUser = function(form) {
            var checkData = $scope.data;
            $scope.addChecklistnote();
            //Checklists.sendChecklistsname(checkName);
            Checklists.saveUserChecklist(checkData)
                .success(function(data, status, headers, config) {
                    console.log(data);
                    $scope.submitted = false;
                    $scope.messages = 'Checkliste erfolgreich gespeichert.';
                })
                .error(function(data) {
                    console.log(data);
                })
            $timeout(function() {
                $scope.messages = null;
            }, 5000);
        };

    });

})();