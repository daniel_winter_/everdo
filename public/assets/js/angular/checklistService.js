angular.module('checklistService', [])

.factory('Checklists', function($http) {

    return {
        //Get data from Checklist
        //get : function(checklistsID) {
          //  return $http.get('../api/checklists/'+checklistsID);
        //},
        //Save a Checklist
        save: function(checkData) {
            return $http({
                method: 'POST',
                url: '../api/checklists',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: checkData
            });
        },
        resave: function(checkData) {
            return $http({
                method: 'POST',
                url: '../../api/checklists',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: checkData
            });
        },
        saveUserChecklist: function(checkData) {
            return $http({
                method: 'POST',
                url: '../../api2/userchecklists',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: checkData
            });
        },
        get: function(checklistsID) {
            return $http({
                method: 'GET',
                url: '../../api/checklists/'+checklistsID,
                //params: {jData:checkData} 
            });
        }
    }
});

var userController = function($scope){
    $scope.users = [];
    
    $scope.addUser = function ($event) {
      $scope.users.push({});
    };
}