$(function() {

        //Change headline Neue Sammlung on keydown
        $('#getCollectionsName').on('input', function() {
            var getCollectionsName = this.value;
            $('#collectionsname').text(getCollectionsName);
            if( $(this).val().length === 0) {
                $('#collectionsname').text("Neue Sammlung");
            }
        });

        //Change headline Neuer Entwurf on keydown       
        $('#checklistsName').on('input', function() {
            var getChecklistsName = this.value;
            $('#checklistsname').text(getChecklistsName);
            if( $(this).val().length === 0) {
                $('#checklistsname').text("Neuer Entwurf");
            }
        });
 
        // Confirm deleting resources
        $("form[data-confirm]").submit(function() {
	        if ( ! confirm($(this).attr("data-confirm"))) {
	                return false;
	        }
        });

        //tab for switching between checklists pattern and settings
        $('#checklistsPattern').on('click', function() {
                $('#contentPattern').show();
                $(this).addClass('active');
                $('#checklistsSettings').removeClass();
                $('#contentSettings').hide();
        });

        $('#checklistsSettings').on('click', function() {
                $('#contentSettings').show();
                $(this).addClass('active');
                $('#checklistsPattern').removeClass();
                $('#contentPattern').hide();
        });


        //tab for switching between collections and patterns
        $('#getAllPattern').on('click', function() {
                $('#contentAllPattern').show();
                $(this).addClass('active');
                $('#getAllCollections').removeClass();
                $('#contentAllCollections').hide();
        });

        $('#getAllCollections').on('click', function() {
                $('#contentAllCollections').show();
                $(this).addClass('active');
                $('#getAllPattern').removeClass();
                $('#contentAllPattern').hide();
        });


        $(document).on('click', '.panel-heading span.clickable', function(e){
            var $this = $(this);
                if(!$this.hasClass('panel-collapsed')) {
                        $this.parents('.panel').find('.panel-body').slideUp();
                        $this.addClass('panel-collapsed');
                        $this.find('i').removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
                } else {
                        $this.parents('.panel').find('.panel-body').slideDown();
                        $this.removeClass('panel-collapsed');
                        $this.find('i').removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');
                }
        })

        $('.navbar-toggle').on('click', function() {

        	if (!$(this).attr('data-toggled') || $(this).attr('data-toggled') == 'off'){
               $(this).attr('data-toggled','on');

               $('.logo-header').css('display', 'none');
               $('.navbar-toggle').animate({  borderSpacing: 90 }, {
                    step: function(now,fx) {
                      $(this).css('-webkit-transform','rotate('+now+'deg)'); 
                      $(this).css('-moz-transform','rotate('+now+'deg)');
                      $(this).css('transform','rotate('+now+'deg)');
                    },
                    duration:'slow'
                },'linear');
            }
            else if ($(this).attr('data-toggled') == 'on'){
               $(this).attr('data-toggled','off');

               $('.logo-header').css('display', 'block');
               $('.navbar-toggle').animate({  borderSpacing: 0 }, {
                    step: function(now,fx) {
                      $(this).css('-webkit-transform','rotate('+now+'deg)'); 
                      $(this).css('-moz-transform','rotate('+now+'deg)');
                      $(this).css('transform','rotate('+now+'deg)');
                    },
                    duration:'slow'
                },'linear');
            }
        });

        $(window).resize(function() {
            if ($(window).width() > 992) { 
                $('.logo-header').css('display', 'none');
            }
            else {
                $('.logo-header').css('display', 'block');

            }
        });

        $(document).on("click", function() {
            $(this).attr('data-toggled','off');

               // $('.logo-header').css('display', 'block');
               $('.navbar-toggle').animate({  borderSpacing: 0 }, {
                    step: function(now,fx) {
                      $(this).css('-webkit-transform','rotate('+now+'deg)'); 
                      $(this).css('-moz-transform','rotate('+now+'deg)');
                      $(this).css('transform','rotate('+now+'deg)');
                    },
                    duration:'slow'
                },'linear');
        });

});