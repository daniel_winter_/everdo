<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

//about-us site
Route::get('/about', function()
{
	return View::make('about');
});

Route::get('/', function()
{
	//TODO
	$usersID = Auth::id();
	$settingsID = DB::table('users')->where('usersID', $usersID)->pluck('settingsID');

	$settingsLogo = DB::table('settings')
		->select('settingslogo')
		->where('settingsID', '=', $settingsID)
		->pluck('settingslogo');

	$getUserSettingsLogo = url('/assets/images/logo/'.$settingsLogo);

	return View::make('login', array('getUserSettingsLogo' => $getUserSettingsLogo));
});

Route::get('login', array('as' => 'login', 'uses' => 'LoginController@showLogin'));

Route::get('help', array('as' => 'help', 'uses' => 'SettingsController@showHelp'));

Route::post('login', array('as' => 'login', 'uses' => 'LoginController@handleLogin'));

Route::get('register', array('as' => 'register', 'uses' => 'LoginController@showRegister'));
//Route::post('register', array('as' => 'register', 'uses' => 'LoginController@store'));
Route::resource('reg', 'LoginController'); 

Route::get('logout', array('as' => 'logout', 'uses' => 'LoginController@logout'))->before('auth');

//Route fuer die E-Mail Registrierung
Route::get('register/verify/{confirmationCode}', ['as' => 'confirmation_path','uses' => 'LoginController@confirm']);

//Route fuer Password vergessen
Route::get('forgetpassword', ['as' => 'forgetpassword','uses' => 'LoginController@forgetpassword']);
Route::post('getPassword', 'LoginController@getPassword'); 

Route::group(array('before'=>'auth'), function() { 
	Route::resource('users', 'UsersController');
    Route::resource('checklists', 'ChecklistsController');
	Route::resource('collections', 'CollectionsController');
	Route::resource('assignments', 'AssignmentsuserschecklistsController');
	// Route::post('storeassignment', ['as' => 'storeassignment','uses' => 'AssignmentsuserschecklistsController@storeAssignment']);
	Route::resource('tasks', 'TasksController');
	Route::resource('settings', 'SettingsController');
	Route::resource('dashboard', 'DashboardController', array('only' => array('index', 'show')));
	Route::get('changepassword', ['as' => 'changepassword','uses' => 'UsersController@changepassword']);
	Route::post('updatepassword', ['as' => 'updatepassword','uses' => 'UsersController@updatepassword']);

	Route::get('created', ['as' => 'created','uses' => 'ChecklistsController@allChecklists']);
	Route::get('open', ['as'=> 'open','uses' => 'ChecklistsController@openChecklists']);
	Route::get('inprogress', ['as' => 'inprogress','uses' => 'ChecklistsController@inProgressChecklists']);
	Route::get('closed', ['as' => 'closed','uses' => 'ChecklistsController@closedChecklists']);
});

Route::post('add', array('as' => 'add','uses' => 'CollectionsController@add'));

//Route is defined on localhost/checklist/public/api/checklists
Route::group(array('prefix' => 'api', 'before'=>'auth'), function() {

	// since we will be using this just for CRUD, we won't need create and edit
	// Angular will handle both of those forms
	// this ensures that a user can't access api/create or api/edit when there's nothing there
	Route::resource('checklists', 'AngCheckController', 
		array('only' => array('index', 'store', 'show', 'edit')));
});

Route::group(array('prefix' => 'api2', 'before'=>'auth'), function() {

	// since we will be using this just for CRUD, we won't need create and edit
	// Angular will handle both of those forms
	// this ensures that a user can't access api2/create or api2/edit when there's nothing there
	Route::resource('userchecklists', 'AngUserController', 
		array('only' => array('index', 'store', 'show', 'edit')));
});

// =============================================
// CATCH ALL ROUTE =============================
// =============================================
// all routes that are not home or api will be redirected to the frontend
// this allows angular to route them
App::missing(function($exception)
{
	//return View::make('login');
});
