<!Doctype html>
<html ng-app="checklistApp">
	<head>
		@include('includes.head')
	</head>
	<body class="main-bg">

		
		@yield('content')
		@include('includes.footer')
	</body>
</html>