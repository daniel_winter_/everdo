<!Doctype html>
<html ng-app="checklistApp">
	<head>
		@include('includes.head')
	</head>
	<body style="background: white; overflow-x: hidden;">

		@if (Auth::user()->rolesID == 2)

			<div class="contentUser">
			 	<div style="position:relative; left: -300px;">
			 		@include('includes.navbar')
			 	</div>
				@yield('content')
				@include('includes.footer')
		    </div>
		



		@else

			@include('includes.sidebar')

			@include('includes.navbar')

		    <div class="containerMain">
				@yield('content')
		    </div>
			
			@include('includes.footer')


		@endif


	
	</body>
</html>



