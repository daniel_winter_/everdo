@extends('layouts.default')
@section('content')

<div class="container">
    <div class="row">
        <!-- Form Name -->
            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 col-md-offset-2 col-sm-offset-2">
                <!-- <center><h1 class="form-signin-heading text-muted">Login</h1></center> -->
                <div class="inner-bg">
                    <div class="row">
                        <div>
                            <div class="center logo">
                                <a href="{{URL::to('about')}}" title="Über Everdo"><img src="assets/images/logo/default.svg" class="logo-img"></a>
                            </div>
                        </div>
                        
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="arrow_box">
                                @if($errors->has())
                                    <div class="alert alert-danger alert-login">
                                        @foreach ($errors->all() as $message)
                                            <p>{{$message}}</p>
                                        @endforeach
                                    </div>
                                @endif
                                {{ Form::open(array('action' => array('LoginController@getPassword'), 'method' => 'post','class' => 'form-horizontal')) }}
                                <!-- E-Mail input-->
                                <div class="input-group">
                                	<span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                    <div>
                                        {{Form::text('email', null,array('class' => 'form-control','placeholder' => 'E-Mail'))}}
                                    </div>
                                </div>
                                
                                <!-- Button -->
                                <div class="btn-login">
                                    <button type="submit" class="btn btn-default btn-gray btn-login"><i class="fa fa-check icon-white"></i>Neues Passwort anfordern</a></button>
                                </div>
                                
                                <!-- Links -->              
                                <div class="btn-login">
                                    <p class="pull-right">{{ HTML::linkRoute('login', 'Zurück zum Login', array(), array('class' => 'login-link')) }}</p>
                                </div>

                                {{ Form::close() }}
                            </div>
                            <!-- Footer Menu -->
                                <div style="clear:both;"></div>
                                <div class="footerMenu">
                                    <ul>
                                        <li><a href="http://flock-0686.students.fhstp.ac.at/" title="Über Everdo">Über everdo</a></li>
                                    </ul>
                                </div>
                        </div>

                    </div>
                    
                </div>
            </div> 
    </div>
</div>

@stop
