<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
		<!-- BASICS -->
        <meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>everdo - about us</title>
        <meta name="description" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="stylesheet" type="text/css" href="about-us/css/isotope.css" media="screen" />	
		<link rel="stylesheet" href="about-us/js/fancybox/jquery.fancybox.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="about-us/css/bootstrap.css">
		<link rel="stylesheet" href="about-us/css/bootstrap-theme.css">
        <link rel="stylesheet" href="about-us/css/style.css">
		<!-- skin -->
		<link rel="stylesheet" href="about-us/skin/default.css">
		<link rel="icon" href="about-us/img/favicon.ico" type="image/x-icon">
    </head>
	 
    <body>
		<section id="header" class="appear"></section>
		<div class="navbar navbar-fixed-top" role="navigation" data-0="line-height:100px; height:100px; background-color:rgba(0,0,0,0.6);" data-300="line-height:60px; height:60px; background-color:rgba(61,61,63,1);">
			 <div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
						<span class="fa fa-bars color-white"></span>
					</button>
					<div><img src="about-us/img/LogoOhneSchrift.png" width="37" height="41"></div>
				</div>

				<div class="navbar-collapse collapse">
					<ul class="nav navbar-nav" data-0="margin-top:20px;" data-300="margin-top:5px;">
						<li class="active"><a href="#header">Home</a></li>
						<li><a href="#section-works">Über everdo</a></li>
						<li><a href="#section-feature">Features</a></li>
						<li><a href="#section-about">Team</a></li>
						<li><a href="#section-contact">Kontakt</a></li>
						<li><a href="#section-impressum">Impressum</a></li>
						<li><a href="./" style="background: #41ad78; border-radius: 5px;" title="Jetzt registrieren">Jetzt registrieren!</a></li>
					</ul>
				</div><!--/.navbar-collapse -->
			</div>
		</div>


		<!-- home -->
		<section class="featured">
			<div class="container">
				<img src="about-us/img/responsive_everdo.png" alt="" />
			</div>
		</section> <!-- /home -->

		<!-- spacer section:testimonial -->
		<section id="section-works" class="testimonials section" style="margin-bottom: 0;" data-stellar-background-ratio="0.5">
			<div class="container">
				<div class="row">				
					<div class="col-lg-12">
						<div class="align-center">
							<div class="testimonial pad-top40 pad-bot40 clearfix">
								<div class="align-center pad-top40 pad-bot40">
                					<blockquote class="bigquote color-white">Über everdo</blockquote>
            					</div>
							</div>
						</div>
					</div>
				</div>	
			</div>	
		</section> <!-- /spacer section:testimonial -->

		
		<section style="margin-bottom: 60px;">
			<!--<iframe src="//player.vimeo.com/video/105216332?title=0&portrait=0&color=41ad78" width="100%" height="500" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen title="0"></iframe> -->
			<iframe src="//player.vimeo.com/video/117952699?title=0&portrait=0&color=41ad78" width="100%" height="500" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen title="0"></iframe> 
		</section>

		<!-- works -->
		<section id="section-works" class="section appear clearfix">
			<div class="container">
				<div class="row mar-bot40">
					<div class="col-md-12">
						<div class="section-header">
							<!-- <h2 class="section-heading animated" data-animation="bounceInUp">Was ist everdo.?</h2> -->
							<div class="row">
								<div class="col-md-6">
									<i class="fa fa-mobile fa-3x mar-bot15"></i>
									<h3>
										Mobile First Applikation
									</h3>
									<p>
										Optimale Nutzung auf dem Smartphone oder Tablet-PC.
									</p>
									<br/>
									<i class="fa fa-check-square-o fa-2x mar-bot15"></i>
									<h3>
										Tool zur Verwaltung von Checklisten
									</h3>
									<p>
										Checklisten und Tasks können individuell angelegt werden und erleichtern so strukturiertes Arbeiten.
									</p>
									<br/>
									<i class="fa fa-users fa-2x mar-bot15"></i>
									<h3>
										Optimal für gemeinnützige Organisationen und Vereine
									</h3>
									<p>
										Durch die Zusammenarbeit mit einer bekannten gemeinnützigen Organisation, sowie einem Fußballverein konnten wir gezielt auf die Bedürfnisse dieser eingehen.
									</p>
									<br/>
								</div>
								<div class="col-md-6">
									<div class="iphone" >
										<img src="about-us/img/iphone1.png" alt="" />
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section> <!-- /works -->


		<!-- spacer section:testimonial -->
		<section id="section-feature" class="testimonials section" data-stellar-background-ratio="0.5">
			<div class="container">
				<div class="row">				
					<div class="col-lg-12">
						<div class="align-center">
							<div class="testimonial pad-top40 pad-bot40 clearfix">
								<div class="align-center pad-top40 pad-bot40">
                					<blockquote class="bigquote color-white">Warum everdo?</blockquote>
									<p class="color-white">
										
									</p>
            					</div>
							</div>
						</div>
					</div>
				</div>	
			</div>	
		</section> <!-- /spacer section:testimonial -->

		
		<!-- works -->
		<section id="section-feature" class="section appear clearfix">
			<div class="container">
				<div class="row mar-bot40">
					<div class="col-md-12">
						<div class="section-header">
							<!-- <h2 class="section-heading animated" data-animation="bounceInUp">Was ist everdo.?</h2> -->
							<div class="col-md-6">
								<div class="iphone" >
									<img src="about-us/img/iphone2.png" alt="" />
								</div>
							</div>
							<div class="col-md-6">
								<br />
								<br />
								<i class="fa fa-mobile fa-3x mar-bot15"></i>
								<i class="fa fa-laptop fa-3x mar-bot15"></i>
								<h3>
									Optimierung für Mobile- und Desktopbereich
								</h3>
								<p>
									Die Konfiguration erfolgt übersichtlich am Desktop-PC oder Laptop, das Abarbeiten bequem und praktisch am Smartphone oder Tablet.
								</p>
								<br/>
								<i class="fa fa-users fa-2x mar-bot15"></i>
								<h3>
									Verschiedene Benutzergruppen - Admin und User
								</h3>
								<p>
									Der Administrator stellt alles nötige zur Verfügung, damit der User nur noch seine Tasks abzuarbeiten hat.
								</p>
								<br/>
								<i class="fa fa-leaf fa-2x mar-bot15"></i>
								<h3>
									Ersatz von Papierchecklisten
								</h3>
								<p>
									Umweltfreundlich, da mit everdo. keine Papierchecklisten mehr nötig sind. Außerdem platzsparend, da nun alles in einer Datenbank hinterlegt ist.
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section> <!-- /works -->


		<!-- spacer section:testimonial -->
		<section id="section-feature" class="testimonials section" data-stellar-background-ratio="0.5">
			<div class="container">
				<div class="row">				
					<div class="col-lg-12">
						<div class="align-center">
							<div class="testimonial pad-top40 pad-bot40 clearfix">
								<div class="align-center pad-top40 pad-bot40">
                					<blockquote class="bigquote color-white">Features</blockquote>
									<p class="color-white">
										
									</p>
            					</div>
							</div>
						</div>
					</div>
				</div>	
			</div>	
		</section> <!-- /spacer section:testimonial -->

		
		<!-- feature -->
		<section id="section-feature" class="section appear clearfix">
			<div class="container">
				<div class="row mar-bot40">
					<div class="col-md-12">
						<div class="section-header">
							<!-- <h2 class="section-heading animated" data-animation="bounceInUp">Was ist everdo.?</h2> -->
							<div class="col-md-6">
								<i class="fa fa-users fa-2x mar-bot15"></i>
								<h3>
									Listen teilen
								</h3>
								<p>
									Listen können mit jeweils ausgewählten Mitgliedern und Mitarbeitern geteilt werden.
								</p>
								<br/>
							</div>
							<!-- <div class="col-md-4">
								<i class="fa fa-cloud fa-2x mar-bot15"></i>
								<h3>
									Synchronisation in Echtzeit
								</h3>
								<p>
									Die Listen synchronisieren sich automatisch zu jedem Zeitpunkt.
								</p>
								<br/>
							</div> -->
							<div class="col-md-6">
								<i class="fa fa-calendar fa-2x mar-bot15"></i>
								<h3>
									Fälligkeitsdaten
								</h3>
								<p>
									Fälligkeitsdaten können erstellt werden, um keine Deadlines zu verpassen.
								</p>
								<br/><br/>
							</div>
							<div class="col-md-6">
								<i class="fa fa-mobile fa-2x mar-bot15"></i>
								<i class="fa fa-laptop fa-2x mar-bot15"></i>
								<i class="fa fa-tablet fa-2x mar-bot15"></i>
								<h3>
									Auf allen Geräten
								</h3>
								<p>
									Funktioniert auf allen Geräten: PC, Laptop, Tablet, Smartphone.
								</p>
								<br/>
							</div>
							<div class="col-md-6">
								<i class="fa fa-edit fa-2x mar-bot15"></i>
								<h3>
									Notizen
								</h3>
								<p>
									Notizen können den einzelnen Tasks zugewiesen werden, um z.B. das Verständnis zu erleichtern.
								</p>
								<br/>
							</div>
							<!-- <div class="col-md-4">
								<i class="fa fa-bell fa-2x mar-bot15"></i>
								<h3>
									Benachrichtigungen
								</h3>
								<p>
									Der Administrator erhält bei wichtigen Tasks eine E-Mail-Benachrichtigung über den Status der abarbeitung.
								</p>
								<br/>
							</div> -->
						</div>
					</div>
				</div>
			</div>
		</section> <!-- /overview -->

		
		

		<!-- spacer section:testimonial -->
		<section class="testimonials section" data-stellar-background-ratio="0.5">
			<div class="container">
				<div class="row">				
					<div class="col-lg-12">
						<div class="align-center">
							<div class="testimonial pad-top40 pad-bot40 clearfix">
								<div class="align-center pad-top40 pad-bot40">
                					<blockquote class="bigquote color-white">Team</blockquote>
									<p class="color-white">
										
									</p>
            					</div>
							</div>
						</div>
					</div>
				</div>	
			</div>	
		</section><!-- /spacer section:testimonial -->

			
		<!-- about -->
		<section id="section-about" class="section appear clearfix">
			<div class="container">

				<div class="row align-center mar-bot20">
					<div class="col-md-4">
						<div class="team-member">
							<figure class="member-photo"><img src="about-us/img/team/jungmayr-lisa.jpg" alt="" /></figure>
							<div class="team-detail">
								<h4 class="member">Lisa Jungmayr</h4>
								<span>Frontend / Design</span> <br/>
								<span class="help-block"></span>
								<span class="help-block"></span>
								<br/>
								<br/>
							</div> 
						</div>
					</div>
				
					<div class="col-md-4">
						<div class="team-member">
							<figure class="member-photo"><img src="about-us/img/team/rieger-anna.jpg" alt="" /></figure>
							<div class="team-detail">
								<h4 class="member">Anna Rieger</h4>
								<span>Frontend / Design</span> <br/>
								<span class="help-block"></span>
								<span class="help-block"></span>
								<br/>
								<br/>
							</div>
						</div>
					</div>
				
					<div class="col-md-4">
						<div class="team-member">
							<figure class="member-photo"><img src="about-us/img/team/suess-martina.jpg" alt="" /></figure>
							<div class="team-detail">
								<h4 class="member">Martina Süß</h4>
								<span>Usability</span> <br/>
								<span class="help-block"></span>
								<span class="help-block"></span>
								<br/>
								<br/>
							</div>
						</div>
					</div>


		
					<div class="col-md-4 col-md-offset-2">
						<div class="team-member">
							<figure class="member-photo"><img src="about-us/img/team/strobl-gerald.jpg" alt="" /></figure>
							<div class="team-detail">
								<h4 class="member">Gerald Strobl</h4>
								<span>Frontend / Backend</span> <br/>
								<span class="help-block"></span>
								<span class="help-block"></span>
								<br/>
								<br/>
							</div>
						</div>
					</div>
					
					<div class="col-md-4">
						<div class="team-member">
							<figure class="member-photo"><img src="about-us/img/team/winter-daniel.jpg" alt="" /></figure>
							<div class="team-detail">
								<h4 class="member">Daniel Winter</h4>
								<span>Frontend / Backend</span> <br/>
								<span class="help-block"></span>
								<span class="help-block"></span>
								<br/>
								<br/>
							</div>
						</div>
						
					</div>

				</div> 
						
			</div>
		</section> <!-- /about -->

		
		<!-- spacer section:stats -->
		<section id="section-contact" class="testimonials section pad-top40 pad-bot40" data-stellar-background-ratio="0.5">
			<div class="container">
            	<div class="align-center pad-top40 pad-bot40">
                	<blockquote class="bigquote color-white">Kontakt</blockquote>
            	</div>
			</div>	
		</section> <!-- /spacer section:stats -->
		
		
		<!-- contact -->
		<section id="section-contact" class="section appear clearfix">
			<div class="container">
				<div class="row mar-bot40">
					<div class="col-md-offset-3 col-md-6">
						<div class="section-header">
							<h2 class="section-heading animated" data-animation="bounceInUp"><!-- <h2>So erreichst du uns...</h2> -->
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-8 col-md-offset-2">
						<div class="cform" id="contact-form">
							<div id="sendmessage">
								Die Nachricht wurde an uns gesendet. Vielen Dank!
							</div>

							<form action="contact/contact.php" method="post" role="form" class="contactForm">
							  	<div class="form-group">
									<label for="name">Name</label>
									<input type="text" name="name" class="form-control" id="name" placeholder="" data-rule="maxlen:4" data-msg="Ihr Name muss aus mindestens vier Zeichen bestehen." />
									<div class="validation"></div>
							  	</div>

							  	<div class="form-group">
									<label for="email">Email</label>
									<input type="email" class="form-control" name="email" id="email" placeholder="" data-rule="email" data-msg="Geben Sie bitte eine gültige E-Mail Adresse ein." />
									<div class="validation"></div>
							  	</div>

							  	<div class="form-group">
									<label for="subject">Betreff</label>
									<input type="text" class="form-control" name="subject" id="subject" placeholder="" data-rule="maxlen:4" data-msg="Ihr Betreff muss aus mindestens vier Zeichen bestehen." />
									<div class="validation"></div>
							  	</div>

							  	<div class="form-group">
									<label for="message">Nachricht</label>
									<textarea class="form-control" name="message" rows="5" data-rule="required" data-msg="Wie können wir Ihnen weiterhelfen, geben Sie uns bitte mehr Details?"></textarea>
									<div class="validation"></div>
							  	</div>
							  
							  	<button type="submit" class="btn btn-theme pull-left">Nachricht absenden</button>
							</form>
						</div>
					</div>
				</div>
			</div>
		</section> <!-- /contact -->


		<!-- spacer section:testimonial -->
		<section id="section-impressum" class="testimonials section pad-top40 pad-bot40" data-stellar-background-ratio="0.5">
			<div class="container">
            	<div class="align-center pad-top40 pad-bot40">
                	<blockquote class="bigquote color-white">Impressum</blockquote>
            	</div>
			</div>	
		</section> <!-- /spacer section:stats -->

		
		<!-- imprint -->
		<section id="section-impressum" class="section appear clearfix">
			<div class="container">
				<div class="row mar-bot40">
					<div class="col-md-12">
						<div class="section-header">
							<!-- <h2 class="section-heading animated" data-animation="bounceInUp">Was ist everdo.?</h2> -->
							<div class="row">
								<div class="col-md-6 col-md-offset-3">
									<p>
										everdo ist ein Projekt von Studierenden der Fachhochschule St. Pölten, das im Rahmen des Projektsemesters in der Spezialisierung “Interaktive Medien” des Bachelorstudiums Medientechnik entstanden ist.
									</p>
									<br/>
									<h3>
										Technische Umsetzung
									</h3>
									<p>
										<br/>
										Lisa Jungmayr <br/>
										Anna Rieger <br/>
										Gerald Strobl <br/>
										Martina Süß <br/>
										Daniel Winter <br/>
									</p>
									<p>
										Fachhochschule St. Pölten GmbH <br/>
										Matthias Corvinus-Straße 15 <br/>
										3100 St. Pölten <br/>

										Die inhaltliche Verantwortung liegt bei den Mitgliedern der Projektgruppe.
									</p>
									<br/>
									<h3>
										Projektbetreuung und Unterstützung
									</h3>
									<p>
										<br/>
										FH-Prof. Dipl.-Ing. Markus Seidl Bakk.. rer.oec.soc. <br/>
										Dipl.-Ing. Ewald Wieser BSc <br/>
										Teresa Sposato Bakk. <br/>
										<br/>
									</p>
									<h3>
										Haftung
									</h3>
									<p>
										<br/>
										Sämtliche Inhalte werden sorgfältig recherchiert und geprüft. Dessen ungeachtet kann keine Garantie für die Richtigkeit, Vollständigkeit und Aktualität der Angaben übernommen werden. Die Inhalte repräsentieren die Meinungen der Autoren und nicht notwendigerweise die offizielle Meinung der Fachhochschule St. Pölten. Eine Haftung der Fachhochschule St. Pölten wird daher ausgeschlossen. Die Links zu anderen Webseiten wurden sorgfältig ausgewählt. Da die Fachhochschule St. Pölten auf deren Inhalt keinen Einfluss hat, übernimmt die Fachhochschule St. Pölten dafür keine Verantwortung.
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section> <!-- /works -->

		
		<!-- footer -->
		<section id="footer" class="section footer">
			<div class="container">
				<!-- <div class="row animated opacity mar-bot20" data-andown="fadeIn" data-animation="animation">
					<div class="col-sm-12 align-center">
                    	<ul class="social-network social-circle">
                        	<li><a href="https://www.facebook.com/anton.everdo" class="icoFacebook" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                    	</ul>				
					</div>
				</div> -->

				<div class="row align-center copyright">
					<div class="col-sm-12"><p style="color:white;">Copyright &copy; 2014</p></div>
				</div>
			</div>
		</section> <!-- /footer -->


		<a href="#header" class="scrollup"><i class="fa fa-chevron-up"></i></a>	

		<script src="about-us/js/modernizr-2.6.2-respond-1.1.0.min.js"></script>
		<script src="about-us/js/jquery.js"></script>
		<script src="about-us/js/jquery.easing.1.3.js"></script>
	    <script src="about-us/js/bootstrap.min.js"></script>
		<script src="about-us/js/jquery.isotope.min.js"></script>
		<script src="about-us/js/jquery.nicescroll.min.js"></script>
		<script src="about-us/js/fancybox/jquery.fancybox.pack.js"></script>
		<script src="about-us/js/skrollr.min.js"></script>		
		<script src="about-us/js/jquery.scrollTo-1.4.3.1-min.js"></script>
		<script src="about-us/js/jquery.localscroll-1.2.7-min.js"></script>
		<script src="about-us/js/stellar.js"></script>
		<script src="about-us/js/jquery.appear.js"></script>
		<script src="about-us/js/validate.js"></script>
	    <script src="about-us/js/main.js"></script>
	    <script type="text/javascript">
	    $( document ).ready(function() {
		    $(".nav a").on("click", function(){
			   $(".nav").find(".active").removeClass("active");
			   $(this).parent().addClass("active");
			});
		});

	    </script>
	</body>
</html>