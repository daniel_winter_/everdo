<div class="navmenu navmenu-default navmenu-fixed-left offcanvas-sm">
	<div class="list-group">
		
		<a href="{{ URL::to('dashboard') }}" class=""><img src="{{ $getUserSettingsLogo }}" style="background-size:cover; max-height: 145px; min-height: 145px; max-width: 145px; display: block; margin: 0 auto; padding: 20px 0 15px 0;"></a>

		<ul><li {{ (Request::is('*dashboard') ? 'class="activeLink"' : '') }}><a href="{{ URL::to('dashboard') }}" class="list-group-item"><i class="fa fa-home icon-gray"></i>Neuigkeiten</a></li></ul>
			
		<ul>
			<p class="heading">Ansehen</p>
			<li {{ (Request::is('*created') ? 'class="activeLink"' : '') }}>
				<a href="{{ URL::to('created') }}" class="list-group-item"><i class="fa fa-database icon-gray"></i>Alle Checklisten <span class="badge">{{ $allChecklists }}</span></a>
			</li>
			<li {{ (Request::is('*open') ? 'class="activeLink"' : '') }}>
				<a href="{{ URL::to('open') }}" class="list-group-item"><i class="fa fa-circle-o icon-gray"></i>Offen <span class="badge">{{ $openChecklists }}</span></a>
			</li>
			<li {{ (Request::is('*inprogress') ? 'class="activeLink"' : '') }}>
				<a href="{{ URL::to('inprogress') }}" class="list-group-item"><i class="fa fa-adjust icon-gray"></i>In Bearbeitung <span class="badge">{{ $inProgressChecklists }}</span></a>
			</li>
			<li {{ (Request::is('*closed') ? 'class="activeLink"' : '') }}>
				<a href="{{ URL::to('closed') }}" class="list-group-item"><i class="fa fa-circle icon-gray"></i>Erledigt <span class="badge">{{ $closedChecklists }}</span></a>
			</li>
		</ul>
		
		<ul>
			<p class="heading">Verwalten</p>
			<li {{ (Request::is('*checklists') ? 'class="activeLink"' : '') }}>
				<a href="{{ URL::to('checklists') }}" class="list-group-item"><i class="fa fa-align-justify icon-gray"></i>Alle Entwürfe</a>
			</li>
			<li {{ (Request::is('*checklists/create') ? 'class="activeLink"' : '') }}><a href="{{ URL::to('checklists/create') }}" class="list-group-item"><i class="fa fa-file icon-gray"></i>Neuer Entwurf</a></li>
			<li {{ (Request::is('*collections/create') ? 'class="activeLink"' : '') }}><a href="{{ URL::to('collections/create') }}" class="list-group-item"><i class="fa fa-folder icon-gray"></i>Neue Sammlung</a></li>
		</ul>
		
		<ul>
			<p class="heading">Benutzer</p>
			<li {{ (Request::is('*users') ? 'class="activeLink"' : '') }}><a href="{{ URL::to('users') }}" class="list-group-item"><i class="fa fa-users icon-gray"></i>Alle Benutzer</a></li>
			<li {{ (Request::is('*users/create') ? 'class="activeLink"' : '') }}><a href="{{ URL::to('users/create') }}" class="list-group-item"><i class="fa fa-user icon-gray"></i>Neuer Benutzer</a></li>
		</ul>

		<ul>
			<p class="heading">Hilfe</p>
			<li {{ (Request::is('*help') ? 'class="activeLink"' : '') }}><a href="{{ URL::to('help') }}" class="list-group-item"><i class="fa fa-question-circle icon-gray"></i>Erste Schritte</a></li>
		</ul>
		
		<!--
		Unpublish
		<ul>
			<p class="heading">Einstellungen</p>
			<li {{ (Request::is('*settings') ? 'class="activeLink"' : '') }}><a href="{{ URL::to('settings') }}" class="list-group-item"><i class="fa fa-cog icon-gray"></i>Seite anpassen</a></li>
		</ul>-->
	</div>
</div>
