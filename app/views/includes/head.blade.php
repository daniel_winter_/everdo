<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
		
<title>everdo.</title>
<!-- <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet"> -->
<link href='http://fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>

<link rel="icon" href="assets/images/favicon.ico" type="image/x-icon">
{{HTML::script('assets/js/jquery-1.11.1.min.js')}}
{{HTML::script('assets/js/moment.js')}}
{{HTML::script('assets/js/jquery.circliful.js')}}
{{HTML::style('assets/css/bootstrap.min.css')}}
{{HTML::style('assets/css/bootstrap.icon-large.css')}}
{{HTML::style('assets/css/jasny-bootstrap.min.css')}}
{{HTML::style('assets/css/main.css')}}
{{HTML::style('assets/css/styles.css')}}
{{HTML::style('assets/css/checklist_app.css')}}
{{HTML::style('assets/css/font-awesome.min.css')}}
{{HTML::style('assets/css/navmenu.css')}}
{{HTML::style('assets/css/angular-ui-tree.min.css')}}
{{HTML::style('assets/css/bootstrap-datetimepicker.min.css')}}


<!-- TODO
	<link rel="stylesheet" type="text/css" media="screen" href="assets/css/settings.php"> -->
<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
		
<!--[if lt IE 10]>
	<link href="css/ie_hacks.css" rel="stylesheet">
<![endif]-->	
