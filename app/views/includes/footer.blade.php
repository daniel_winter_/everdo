{{HTML::script('assets/js/jquery.js')}}
{{HTML::script('assets/js/angular/angular.min.js')}}
{{HTML::script('assets/js/angular/angular-messages.js')}}
{{HTML::script('assets/js/angular/angular-ui-tree.js')}}
{{HTML::script('assets/js/angular/checklistService.js')}}
{{HTML::script('assets/js/angular/promise-tracker.js')}}
{{HTML::script('assets/js/angular/tree-checklist.js')}}
{{HTML::script('assets/js/ms_app.js')}}
{{HTML::script('assets/js/bootstrap.min.js')}}
{{HTML::script('assets/js/jasny-bootstrap.min.js')}}
{{HTML::script('assets/js/bootstrap-datetimepicker.min.js')}}
{{HTML::script('assets/js/bootstrap-datetimepicker.de.js')}}
{{HTML::script('assets/js/jquery.circliful.js')}}

<script type="text/javascript">
	$(document).ready(function() {
	//Frist Entwurf bearbeiten
    $('.datepicker').datetimepicker({
		language: 'de',
		pickTime: false,
		minDate: moment()
	});
    //reminder Entwurf bearbeiten
	$('.timepicker').datetimepicker({
		language: 'de',
		pickDate: false
	});

	$(function () {
	  $('[data-toggle="tooltip"]').tooltip()
	});

});
</script>
