<div ng-init="loadChecklists(<?php echo $checklists[0]->checklistsID ?>)">
	
	<!--angular messages-->
	<div class="alert alert-success" data-ng-show="messages" data-ng-bind="messages"></div>
	<div ng-show="submitted" ng-messages="checkForm.checklistsname.$error">
		<!-- Required title of Checklist -->
		<div class="alert alert-danger" ng-message="required">Bitte geben Sie einen Titel an.</div>
		<!-- Minlength checklist -->
		<div class="alert alert-danger" ng-message="minlength">Der Titel muss mindestens 5 Zeichen lang sein.</div>
	</div>
	<div class="alert alert-danger" data-ng-show="messagestask" data-ng-bind="messagestask"></div>
	<form name="checkForm" novalidate role="form">
		<!--Titel der Checkliste-->
		<div class="group-title col-lg-12">
			<div class="row">
				<div class="form-group">
					<input type="text" name="checklistsname" ng-minlength="5" maxlength="30" class="form-control input-lg" id="checklistsName" ng-model="checkNameModel" placeholder="Titel des Entwurfes *" ng-init="checkNameModel='<?php echo $checklists[0]->checklistsname ?>'"/>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="input-group">
				<input type="text" maxlength="40" class="form-control" id="taskName" ng-model="newTasks" placeholder="Checkpunkt eingeben" required>
				<span class="input-group-addon">
				<button type="submit" name="tasks"class="btn btn-default btn-plus" nodrag ng-click="addTasks(checktask)"><i class="fa fa-plus"></i></span>
			</div>
		</div>
		<!-- Nested node template -->
		<script type="text/ng-template" id="nodes_renderer.html">
		<div ui-tree-handle class="tree-node tree-node-content">
			<a ng-show="filtershow(this)" class="btn btn-s" ng-if="node.nodes && node.nodes.length > 0" style="font-size:10px;" nodrag ng-click="toggle(this)"><span class="fa" ng-class=" &#123;'fa-chevron-down': collapsed, 'fa-chevron-up': !collapsed &#125;"></span></a>
						
<!--Icons: Notiz, Edit, Delete-->	
			<a ng-show="filtershow(this)"><%node.tasksname%><a style="font-style:italic; font-size: 10px;"ng-show="node.tasksnote != null && filtershow(this)"> <%node.tasksnote%></a></a>
			<a ng-show="filtershow(this)" class="btn btn-s pull-right icon-tasks" nodrag ng-click="remove(this)"><span class="fa fa-trash icon-nav"></span></a>
			<a ng-show="filtershow(this)" class="btn btn-s pull-right icon-tasks" nodrag ng-click="editNote(this)"><span class="fa fa-comment icon-nav"></span></a>
			<a ng-show="filtershow(this)" class="btn btn-s pull-right icon-tasks" nodrag ng-click="editTask(this)"><span class="fa fa-pencil icon-nav"></span></a>
			

<!--Checkpunkt editieren-->
			<div class="row" nodrag ng-show="node.editing">
				<div class="input-group input-edit" role="form">
					<input type="text" class="form-control" id="taskRename" placeholder="Checkpunktname editieren" ng-model="node.tasksname">
					<span class="input-group-addon">
					<button type="submit" class="btn btn-default btn-plus" ng-click="cancelEditingTask(this)"><i class="fa fa-check icon-nav"></i></button></span>
				</div>
			</div>

<!--Notiz hinzufügen-->
			<div nodrag ng-show="node.note_editing">
				<div class="input-group input-addnote" role="form">
					<input type="text" class="form-control" id="noteEdit" placeholder="Notiz hinzufügen" ng-model="node.tasksnote">
					<span class="input-group-addon">	
					<button type="submit" class="btn btn-default btn-plus" ng-click="cancelNoteTask(this)"><i class="fa fa-check icon-nav"></i></button></span>
				</div>
			</div>
					
					
		</div>
			<ol class="tasks" data-everdo="" ui-tree-nodes="" ng-model="node.nodes" ng-class="{hidden: collapsed}">
					<li class="tasks"  data-everdo="" ng-repeat="node in node.nodes" ui-tree-node ng-include="'nodes_renderer.html'">
					</li>
			</ol>
		</script>

		<!-- task list-->
		<div ui-tree id="tree-root" data-max-depth="7">
			<ol ui-tree-nodes ng-model="data">
				<li ng-repeat="node in data" ui-tree-node ng-include="'nodes_renderer.html'"></li>
			</ol>
		</div>
		<!-- end whole content-->
		
		<!-- end task list-->
	<button style="margin-top: 5px;" type="submit" class="btn btn-default btn-gray" ng-click="resaveChecklist(checkForm)"><span class="fa fa-check icon-white"></span>Speichern</a></button>
</form>
</div>
</div>