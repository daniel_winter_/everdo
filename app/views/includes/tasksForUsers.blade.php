<div ng-controller="checklistCtrl" ng-init="loadChecklists(<?php echo $assignments[0]->checklistsID ?>)">
	
	<!--angular messages-->
	<div class="alert alert-success" data-ng-show="messages" data-ng-bind="messages"></div>
	<div class="alert alert-danger" data-ng-show="messagestask" data-ng-bind="messagestask"></div>
	<form name="checkForm" novalidate role="form">
		<!--Titel der Checkliste-->
		

		<style type="text/css">
			.checked {
				background: none repeat scroll 0 0 #b4e0ca !important;
			}
			.nochecked {
				background: none repeat scroll 0 0 #f8f8f8 !important;
			}

			.angular-ui-tree-handle {
				cursor: pointer;
				padding: 5px 0px 5px 10px;
			}

			.user-note {
				margin: 15px 0;
			}

			html body h1 {
				margin-bottom: 10px;
			}
			
		</style>


		<!-- Nested node template -->
		<script type="text/ng-template" id="nodes_renderer.html">
		<div ui-tree-handle class="tree-node tree-node-content" ng-click="itemClicked(this)" ng-class="{'checked' : node.checked, 'nochecked' : !node.checked}" >
			<i ng-show="!node.checked" class="fa fa-square-o"> </i><i ng-show="node.checked" class="fa fa-check-square-o"> </i><a ng-class="{'checked' : node.checked, 'nochecked' : !node.checked}"> <%node.tasksname%><a ng-class="{'checked' : node.checked, 'nochecked' : !node.checked}" style="font-style:italic; font-size: 10px;"ng-show="node.tasksnote != null"> <%node.tasksnote%></a></a>
		</div>
		<ol class="tasks" ui-tree-nodes="" ng-model="node.nodes" ng-class="{hidden: collapsed}">
					<li class="tasks" ng-repeat="node in node.nodes" ui-tree-node ng-include="'nodes_renderer.html'">
					</li>
		</ol>
		</script>
		<!-- task list-->
		<div ui-tree id="tree-root" data-drag-enabled="false">
			<ol ui-tree-nodes ng-model="data">
				<li ng-repeat="node in data"  ui-tree-node ng-include="'nodes_renderer.html'">
				 </li>

			</ol>
			<!-- add note 
			<label for="note" style="font-weight:400;">Notiz hinzufügen:</label>-->
			<input class="form-control ng-valid input-lg user-note" type="text" ng-model="checkNoteModel" ng-init="checkNoteModel='<?php echo $assignments[0]->noteFromUser ?>'" name="note" placeholder="Notiz hinzufügen">
		</div>
		<p></p>
		<!-- end whole content-->
	<button type="submit" class="btn btn-default btn-gray" ng-click="saveChecklistForUser(checkForm)"><i class="fa fa-check icon-white"></i>Speichern</a></button>
</form>
</div>