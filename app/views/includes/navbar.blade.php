<div class="navbar navbar-default navbar-fixed-top hidden-md hidden-lg" id="topnav">
		<!--Sandwich-->
		@if (Auth::user()->rolesID == 2)
			<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 col-lg-offset-2 col-md-offset-2">
				@if(Request::is('assignments'))
				@else
					<button type="button" class="navbar-user-toggle">
						<a href="{{ URL::to('assignments')}}"><span class="fa fa-chevron-left fa-lg"></span></a>
					</button>
				@endif
			<!--Logo-->
			<a href="{{ URL::to('assignments')}}"><img src="{{ url('/assets/images/logo/LogoOhneSchrift') }}.png" class="logoHeaderUser"></a>

		@else
		<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
			<button type="button" class="navbar-toggle" data-toggle="offcanvas" data-target=".navmenu">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<!--Logo-->
			<a href="{{ URL::to('dashboard') }}"><img src="{{ url('/assets/images/logo/LogoOhneSchrift') }}.png" class="logo-header"></a>
		@endif
			

		<!--User-->
		<div class="btn-group username-fixedtop">
			<button type="button" class="dropdown-toggle usernav" data-toggle="dropdown" aria-expanded="false">
				@if (Auth::check())
				<i class="fa fa-user"></i><span class="username-navbar"> {{Auth::user()->username}} </span></a><span class="caret"></span>
			</button>
			<ul class="dropdown-menu pull-right" role="menu">
				<li>{{HTML::linkRoute('changepassword','Passwort ändern')}}</li>
				<li>{{HTML::linkRoute('logout','Abmelden')}}</li>
				@else
				<li>{{HTML::linkRoute('register','Sign Up')}}</li>
				@endif
			</ul>
		</div>
	</div>
</div>