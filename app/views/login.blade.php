@extends('layouts.default')
@section('content')	

<div class="container">
	<div class="row">


		@if( Session::has('activeAccountAlert') )
			<p class="alert alert-success" role="alert"> Bitte checken Sie Ihre E-Mails und aktivieren Sie Ihren Account.</p>
		@endif
		@if( Session::has('successForgetPassword') )
			<p class="alert alert-success" role="alert"> Ihnen wurde ein neues Passwort zugesendet, bitte checken Sie Ihre E-Mails.</p>
		@endif

		<!-- Form Name -->
		<div class="row">
			<div class="col-lg-8 col-md-10 col-sm-8 col-xs-12 col-lg-offset-2 col-md-offset-1 col-sm-offset-2">
				<!-- <center><h1 class="form-signin-heading text-muted">Login</h1></center> -->
				<div class="inner-bg">
					<div class="row">
						<div>
							<div class="center logo">
								<a href="{{URL::to('about')}}" title="Über Everdo"><img src="assets/images/logo/default.svg" class="logo-img"></a>
							</div>
						</div>

						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<div class="arrow_box">
								@if($errors->has())
									<div class="alert alert-danger alert-login" role="alert">
									@foreach ($errors->all() as $error)
										<p> {{ $error }}</p>	
									@endforeach
									</div>
								@endif
								{{ Form::open(array('url' => 'login', 'method' => 'POST','class' => 'form-horizontal', 'role'=>'form')) }}

								<!-- Text input-->
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-user"></i></span>
									<div>
										{{Form::text('username', null, array('class' => 'form-control','placeholder' => 'Benutzername'))}}
									</div>
								</div>

								<!-- Password input-->
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-lock"></i></span>
									<div>
										{{Form::password('password',array('class' => 'form-control','placeholder' => 'Passwort'))}}
									</div>
								</div>
											
								<!-- Button -->
								<div class="btn-login">
									<!--{{Form::submit('Anmelden', array('class' => 'btn btn-lg btn-primary btn-block'))}}-->
									<button type="submit" class="btn btn-default btn-gray btn-login"><i class="fa fa-check icon-white"></i>Anmelden</a></button>

									<!-- <div class="clearfix"></div> -->
									<p class="pull-left text">{{ HTML::linkRoute('forgetpassword', 'Passwort vergessen', array(), array('class' => 'login-link')) }}</p>
									<p class="pull-right">{{ HTML::linkRoute('register', 'Registrieren', array(), array('class' => 'login-link')) }}</p>
								</div>

												
								{{ Form::close() }}
							</div>

								<div style="clear:both;"></div>
                                <div class="footerMenu">
                                    <ul>
                                        <li><a href="{{URL::to('about')}}" title="Über Everdo">Über everdo</a></li>
                                </div>

						</div>

					</div>

				</div>

			</div>					
		</div>
	</div>
</div>

@stop




				