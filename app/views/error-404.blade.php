@extends('layouts.default')
@section('content')	

<div class="container">
	<div class="row">
		<!-- Form Name -->
		<div class="row">
			<div class="col-lg-8 col-md-10 col-sm-8 col-xs-12 col-lg-offset-2 col-md-offset-1 col-sm-offset-2">
				<!-- <center><h1 class="form-signin-heading text-muted">Login</h1></center> -->
				<div class="inner-bg">
					<div>
						<?php
							// $actual_link = $_SERVER['SERVER_NAME'];

							// echo $actual_link;
						?>
						<div class="center logo">
							<img src="/assets/images/logo/default.svg" class="logo-img">
						</div>
					</div>
					<p class="alert alert-danger" role="alert"> neverdo - Die von Ihnen angeforderte Seite gibt es nicht oder ist ungültig. Bitte gehen Sie zurück zur Startseite.
					</p>
				</div>

			</div>					
		</div>
	</div>
</div>

@stop




				