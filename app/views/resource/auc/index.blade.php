@extends('layouts.layout')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 col-lg-offset-2 col-md-offset-2">

			<h1>Checklistenübersicht</h1>
		</div>
	</div>

	<div class="row">
		<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 col-lg-offset-2 col-md-offset-2">
			@if( Session::has('successUpdateAssignment') )
				<p class="alert alert-success" role="alert"> {{ Session::get('successUpdateAssignment') }}</p>
			@endif
		</div>
	</div>

	
	<div class="row">

		<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 col-lg-offset-2 col-md-offset-2">

			@if(empty($assignments))
				<p class="alert alert-info" role="alert"> Zur Zeit gibt es keine Checklisten zum Bearbeiten.</p>
			@else 

				@foreach($assignments as $assign)

				<a href="assignments/{{ $assign->checklistsID }}/edit">
					<div>

						<div class="panel panel-primary">
							<div class="panel-heading">
								<span>{{ $assign->checklistsname}}</span>
							</div>
							<div class="panel-body">
								<i class="fa fa-clock-o icon-overview"></i> Zu erledigen bis: {{ date('d.m.Y', strtotime($assign->duedate)) }} um {{ date('G:i', strtotime($assign->duedate)) }} Uhr
								<p>
								@if(empty($assign->note))
								@else
									<i class="fa fa-comment icon-overview"></i> Notiz: {{ $assign->note }} 
								@endif
							</div>
							<div class="panel-body panel-tasks">
									<span>

										@if($assign->sent == 1)
										{{ Form::open(array('route' => array('tasks.update', $assignments[0]->checklistsID), 'method' => 'PUT', 'data-confirm' => 'Möchtest du die Checkliste wirklich dem Administrator senden?')) }}
										      <button type="submit" href="{{ URL::route('tasks.update', $assignments[0]->checklistsID) }}" class="btn-s fa fa-share btn-sendadmin"><span class="sendadmin"> Senden</span></button>
										{{ Form::close() }}
										@endif
									</span>

								</p>
								
							</div>
						</div>
					</div>
				</a>


				@endforeach
			
			@endif
		
			
		</div>

	</div>
</div>
@stop