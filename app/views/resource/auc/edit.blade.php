@extends('layouts.layout')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 col-lg-offset-2 col-md-offset-2">

			@foreach($assignments as $as)

				<h1>Checkliste: {{ $as->checklistsname }}</h1>
				<i class="fa fa-clock-o icon-overview"></i> Zu erledigen bis: {{ date('d.m.Y', strtotime($as->duedate)) }} um {{ date('G:i', strtotime($as->duedate)) }} Uhr
				<p>
				@if(empty($as->note))
				@else
					<i class="fa fa-comment icon-overview"></i> Notiz: {{ $as->note }} 
				@endif
				</p>

			@endforeach
		</div>
	</div>

	
	<div class="row">
		<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 col-lg-offset-2 col-md-offset-2">
			@include('includes.tasksForUsers')
		</div>
	</div>
</div>
@stop