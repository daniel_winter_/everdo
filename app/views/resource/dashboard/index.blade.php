@extends('layouts.layout')
@section('content')


<div class="container">
	<!-- Form Name -->
	<div class="row">
		<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
			@if( Session::has('successUpdatePassword') )
					<p class="alert alert-success" role="alert"> Passwort erfolgreich geändert.</p>
			@endif

			<h1>Neuigkeiten</h1>
			<div class="row">
				<div class="col-md-12 allcircles" >
					<div class="circlerow">
				<a href="{{ URL::to('created') }}">
						<div class="circle">
						<div id="myStat1" class="single-circle" data-text="{{ $allChecklists }}" data-info="Alle Checklisten" data-width="15" data-percent="<?php echo ($allChecklists > 0) ? $allChecklists  * 100 : '0'; ?>" data-fgcolor="#41AD78" data-bgcolor="#C5C6C8">
						</div>
						<div class="description">
							<span>Alle Checklisten</span>
						</div>
						</div>
				</a>
				<a href="{{ URL::to('open') }}">
					<div class="circle">
						<div id="myStat2" class="single-circle" data-text="{{ $openChecklists }}" data-info="Offen" data-width="15" data-percent="<?php echo ($openChecklists > 0) ? $openChecklists / $allChecklists  * 100 : '0'; ?>" data-fgcolor="#41AD78" data-bgcolor="#C5C6C8">
						</div>
						<div class="description">
							<span>Offen</span>
						</div>
					</div>
				</a>
					</div>
					<div class="circlerow">
				<a href="{{ URL::to('inprogress') }}">
					<div class="circle">
						<div id="myStat3" class="single-circle" data-text="{{ $inProgressChecklists }}" data-info="In Bearbeitung" data-width="15" data-percent="<?php echo ($inProgressChecklists > 0) ? $inProgressChecklists / $allChecklists  * 100 : '0'; ?>" data-fgcolor="#41AD78" data-bgcolor="#C5C6C8">			
						</div>
						<div class="description">
							<span>In Bearbeitung</span>
						</div>
					</div>
				</a>
					<a href="{{ URL::to('closed') }}">
					<div class="circle">
						<div id="myStat4" class="single-circle" data-text="{{ $closedChecklists }}" data-info="Erledigt" data-width="15" data-percent="<?php echo ($closedChecklists > 0) ? $closedChecklists / $allChecklists  * 100 : '0'; ?>" data-fgcolor="#41AD78" data-bgcolor="#C5C6C8">
						</div>
						<div class="description">
							<span>Erledigt</span>
						</div>
					</div>
				</a>
					</div>
				</div>
					<script>
					$('#myStathalf').circliful();
							$('#myStat1').circliful();
							$('#myStat2').circliful();
							$('#myStat3').circliful();
							$('#myStat4').circliful();
					</script>
			</div>	
			
			<div class="container">
					<div class="row">
							<div class="panel panel-primary dash-activity">
								<div class="panel-heading">
									<span>Letzte Aktivitäten</span>
								</div>
								<div class="panel-body">
									@if(empty($lastCreatedCl) && empty($lastCreatedUser))
										<p>Bisher keine Aktivitäten.</p>
									@else 
										@foreach($lastCreatedCl as $lcc)
											<div class="check-overview">
												<div class="activity">Entwurf <strong><a href="checklists/{{ $lcc->checklistsID }}/edit" title="{{ $lcc->checklistsname }}">{{ $lcc->checklistsname }} </a></strong></div>
												<div class="activity">	
													<div class="activity"><i class="fa fa-clock-o icon-overview"></i> erstellt am {{ date('d.m.Y, G:i',strtotime($lcc->created_at)) }} Uhr</div>
													<div class="activity"><i class="fa fa-user icon-overview"></i> von {{ $lcc->username }}.</div>
												</div>
											</div>
										@endforeach
										@if(empty($lastChangedCl))
											
										@else
											@if($lastChangedCl[0]->updated_at == "0000-00-00 00:00:00")
											@else
												@foreach($lastChangedCl as $lchanged)
													<div class="check-overview">
														<div class="activity">Entwurf <strong><a href="checklists/{{ $lchanged->checklistsID }}/edit" title="{{ $lchanged->checklistsname }}">{{ $lchanged->checklistsname }}</a></strong></div>
														<div class="activity">	
															<div class="activity"><i class="fa fa-clock-o icon-overview"></i> bearbeitet am {{ date('d.m.Y, G:i',strtotime($lchanged->updated_at)) }} Uhr</div>
															<div class="activity"><i class="fa fa-user icon-overview"></i> von {{ $lchanged->username }}.</div>
														</div>
													</div>
												@endforeach
											@endif
										@endif

										@foreach($lastDoneCl as $ldcl)
											<div class="check-overview">
												<div class="activity">Entwurf <strong>{{ $ldcl->checklistsname }}</strong></div>
												<div class="activity">
													<div class="activity"><i class="fa fa-clock-o icon-overview"></i> erledigt am {{ date('d.m.Y, G:i',strtotime($ldcl->updated_at)) }} Uhr</div>
													<div class="activity"><i class="fa fa-user icon-overview"></i> von {{ $ldcl->username }}.</div>
												</div>
											</div>
										@endforeach

										@foreach($lastCreatedUser as $lcu)
											<div class="check-overview">
												<div class="activity">Zuletzt angelegter Benutzer: <strong><a href="users/{{ $lcu->usersID }}/edit" title="{{ $lcu->username }}">{{ $lcu->username }}</a></strong></div>
												<div class="activity"><i class="fa fa-clock-o icon-overview"></i>  am {{ date('d.m.Y, G:i', strtotime($lcu->created_at)) }} Uhr.</div>
											</div>
										@endforeach

										<div class="check-overview">Anzahl angelegter Benutzer: <strong>{{ $getNumberUsers }}</strong></div>
									@endif
							</div>
						</div>
					</div>
				</div>
				
		</div>
	</div>
</div>
@stop