@extends('layouts.layout')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
			<h1>Alle Entwürfe
			
			<!-- Filter
			<div class="btn-group pull-right">
				<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
				Filter <span class="caret"></span>
				</button>
				<ul class="dropdown-menu" role="menu">
					<li><a href="#">A-Z</a></li>
					<li><a href="#">Datum</a></li>
				</ul>
			</div>--></h1>
		</div>
	</div>




	<!--Alerts -->
	<div class="row">
		<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
			@if( Session::has('successAddCollection') )
				<p class="alert alert-success" role="alert"> {{ Session::get('successAddCollection') }}</p>
			@endif
			@if( Session::has('successUpdateCollection') )
				<p class="alert alert-success" role="alert"> Sammlung erfolgreich aktualisiert.</p>
			@endif
			@if( Session::has('successDeleteCollection') )
				<p class="alert alert-success" role="alert"> Sammlung erfolgreich gelöscht.</p>
			@endif
			@if( Session::has('successAddChecklist') )
				<p class="alert alert-success" role="alert"> Checkliste erfolgreich angelegt.</p>
			@endif
			@if( Session::has('successUpdateChecklists') )
				<p class="alert alert-success" role="alert"> Checkliste erfolgreich aktualisiert</p>
			@endif
			@if( Session::has('successDeleteChecklists') )
				<p class="alert alert-success" role="alert"> {{ Session::get('successDeleteChecklists') }}</p>
			@endif
			@if( Session::has('successAddAssignment') )
				<p class="alert alert-success" role="alert"> Checkliste erfolgreich dem Benutzer zugewiesen.</p>
			@endif
		</div>
	</div>


	

	<!--Buttons -->
	<div class="row">
		@if(empty($checklists))
			<div class="col-lg-8 col-md-8">
				<p class="alert alert-info" role="alert">Zur Zeit sind keine Entwürfe vorhanden.</p>
			</div>
			<div class="col-lg-8 col-md-8 col-sm-12">
				<a href="checklists/create" class="btn btn-default btn-block btn-add">
					<span>
						<i class="fa fa-plus icon-add"></i></span>
						<p> Neuen Entwurf erstellen</p>
				</a>
			</div>
		@else
			<div class="col-lg-4 col-md-4 col-sm-6">
				<a href="checklists/create" class="btn btn-default btn-block btn-gray">
				<span><i class="fa fa-plus icon-white"></i></span>Neuer Entwurf</a>
			</div>
		@endif
			

		@if(empty($checklists))
		<!-- when there is no checklist, there is no collection button -->
		@else
			<div class="col-lg-4 col-md-4 col-sm-6">
				<a href="collections/create" class="btn btn-default btn-block btn-gray">
				<span><i class="fa fa-plus icon-white"></i></span>Neue Sammlung</a>
			</div>
		@endif	
				
	

		<!--Tabs-->
		@if(empty($checklists))
		@else
			<div class="col-lg-8 col-md-8">
				<ul class="nav nav-pills nav-justified checklists-tab" role="tablist">
				  <li id="getAllPattern" role="presentation" class="active"><a href="#">Entwürfe</a></li>
				  <li id="getAllCollections" role="presentation"><a href="#">Sammlungen</a></li>
				</ul>
			</div>
		@endif
	</div>
	
	<div class="row">
		<!--Checklists -->
		<div id="contentAllPattern">
			<div class="col-lg-4 col-md-4 col-sm-6">
				@foreach($checklists as $iz2 => $key_check)
					@foreach($key_check as $i2 => $key_final)
						<div class="panel panel-primary">
							<div class="panel-body panel-checklists">
								<a href="checklists/{{ $key_final->checklistsID }}/edit">
								<span>{{ $key_final->checklistsname }}</span>
								<span>
								<!--{{ HTML::link('checklists/'.$key_final->checklistsID.'/edit', '', array('class' => 'glyphicon glyphicon-pencil pull-right', 'style' => 'position: relative; top: 4px; left: -35px;')) }}-->

								{{ Form::open(array('route' => array('checklists.destroy', $key_final->checklistsID), 'method' => 'delete', 'style' => 'float: right;', 'data-confirm' => 'Möchtest du diese Checkliste wirklich löschen?')) }}
									{{ Form::hidden('checklistsname', $key_final->checklistsname) }}
								      <button type="submit" href="{{ URL::route('checklists.destroy', $key_final->checklistsID) }}" class="btn-mini fa fa-trash pull-right btnTrash"></button>
								{{ Form::close() }}

								</span>
							</a>
							</div>
						</div>
					@endforeach
				@endforeach
			</div>
		</div>

		<!--Sammlungen-->
		<div id="contentAllCollections">
			<div class="col-lg-4 col-md-4 col-sm-6">
				@foreach($getColValues as $i => $colnamekey)
				<div class="panel panel-primary">
					<div class="panel-heading">
						<a href="collections/{{$colnamekey->collectionsID}}/edit">{{$colnamekey->collectionsname}}</a>
						<span class="pull-right clickable"><i class="glyphicon glyphicon-chevron-up"></i></span>
					</div>
					<div class="panel-body">
						@foreach($collections as $ix => $key1)
							@foreach($key1 as $ix2 => $key2)
							@if($colnamekey->collectionsname == $key2->collectionsname)
							<p>{{ HTML::link('checklists/'.$key2->checklistsID.'/edit', $key2->checklistsname, array('class' => '', 'style' => '')) }}</p>


							@endif
							@endforeach
						@endforeach
					</div>
				</div>
				@endforeach
			</div>
		</div>


	</div>
</div>
@stop