@extends('layouts.layout')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
			<h1>Checklisten in Bearbeitung</h1>
		</div>
	</div>

	
	<div class="row">
		@if(empty($inProgressUserChecklists))
			<div class="col-lg-8 col-md-8 col-sm-12">
				<p class="alert alert-success" role="alert">Zur Zeit gibt es keine Checklisten in Bearbeitung.</p>
			</div>
		@else

			<div class="col-lg-8 col-md-8 col-sm-12">

				<div class="panel panel-primary">
					<div class="panel-heading">
						<span>In Bearbeitung</span>
						<span class="pull-right clickable"><i class="glyphicon glyphicon-chevron-up"></i></span>
					</div>
					<div class="panel-body">
						@foreach($inProgressUserChecklists as $ipuc)
							<!--<p>{{ $ipuc->checklistsname }} (zugewiesen an: <strong>{{ $ipuc->username }})</strong></p>-->
							<div class="check-overview">
								<a href="checklists/{{ $ipuc->checklistsID }}/edit"><div class="check-title">{{ $ipuc->checklistsname }}</div>
								<div class="check-details">
									<div class="check-user"><i class="fa fa-user icon-overview"></i> zugewiesen an: {{ $ipuc->username }}</div>
									<div class="check-date"><i class="fa fa-clock-o icon-overview"></i> zu erledigen bis: {{ date('d.m.Y', strtotime($ipuc->duedate)) }} um {{ date('G:i', strtotime($ipuc->reminder)) }} Uhr</div>
								</div>
								</a>
							</div>
						@endforeach
					</div>
				</div>

			</div>

		@endif
	</div>

</div>
@stop