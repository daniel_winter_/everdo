@extends('layouts.layout')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
			<h1>Erledigte Checklisten</h1>
		</div>
	</div>

	
	<div class="row">
		@if(empty($closedUserChecklists))
			<div class="col-lg-8 col-md-8 col-sm-12">
				<p class="alert alert-success" role="alert">Zur Zeit gibt es keine erledigten Checklisten.</p>
			</div>
		@else

		<div class="col-lg-8 col-md-8 col-sm-12">

			<div class="panel panel-primary">
				<div class="panel-heading">
					<span>Erledigte</span>
					<span class="pull-right clickable"><i class="glyphicon glyphicon-chevron-up"></i></span>
				</div>
				<div class="panel-body">
					@foreach($closedUserChecklists as $cuc)
						<!--<p>{{ $cuc->checklistsname }}(zugewiesen an: <strong>{{ $cuc->username }})</strong></p>-->
						<div class="check-overview">
							<a href="checklists/{{ $cuc->checklistsID }}/edit"><div class="check-title">{{ $cuc->checklistsname }}</div>
								<div class="check-details">
									<div class="check-user"><i class="fa fa-user icon-overview"></i> erhalten von: {{ $cuc->username }}</div>
									<div class="check-user"><i class="fa fa-comment icon-overview"></i> Kommentar von Benutzer: {{ $cuc->noteFromUser }}</div>
								</div>
							</a>
						</div>
					@endforeach
				</div>
			</div>

		</div>
	
		@endif

	</div>

</div>
@stop