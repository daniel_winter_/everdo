@extends('layouts.layout')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
			<h1>Checklistenübersicht</h1>
		</div>
	</div>

	
	<div class="row">

		@if(empty($openUserChecklists) && empty($inProgressUserChecklists) && empty($closedUserChecklists))
			<div class="col-lg-8 col-md-8 col-sm-12">
				<p class="alert alert-success" role="alert">Es wurden noch keine Entwürfe zugewiesen.</p>
			</div>

			<div class="col-lg-8 col-md-8 col-sm-12">
				<a href="checklists/create" class="btn btn-default btn-block btn-add">
					<span>
						<i class="fa fa-plus icon-settings"></i></span>
						<p> Neuen Entwurf erstellen</p>
				</a>
			</div>
		@else
			
			@if(empty($openUserChecklists))
			@else
				<div class="col-lg-8 col-md-8 col-sm-12">

					<div class="panel panel-primary">
						<div class="panel-heading">
							<span>Offene Checklisten</span>
							<span class="pull-right clickable"><i class="glyphicon glyphicon-chevron-up"></i></span>
						</div>
						<div class="panel-body">
							@foreach($openUserChecklists as $ouc)
								<div class="check-overview">
									<a href="checklists/{{ $ouc->checklistsID }}/edit"><div class="check-title">{{ $ouc->checklistsname }}</div>
									<div class="check-details">
										<div class="check-user"><i class="fa fa-user icon-overview"></i> zugewiesen an: {{ $ouc->username }}</div>
										<div class="check-date"><i class="fa fa-clock-o icon-overview"></i> zu erledigen bis: {{ date('d.m.Y', strtotime($ouc->duedate)) }} um {{ date('G:i', strtotime($ouc->reminder)) }} Uhr</div>
									</div>
									</a>
								</div>
							@endforeach
						</div>
					</div>

				</div>
			@endif

			@if(empty($inProgressUserChecklists))
			@else
				<div class="col-lg-8 col-md-8 col-sm-12">

					<div class="panel panel-primary">
						<div class="panel-heading">
							<span>Checklisten in Bearbeitung </span>
							<span class="pull-right clickable"><i class="glyphicon glyphicon-chevron-up"></i></span>
						</div>
						<div class="panel-body">
							@foreach($inProgressUserChecklists as $ipuc)
							<!--<p>{{ $ipuc->checklistsname }} (zugewiesen an: <strong>{{ $ipuc->username }})</strong></p>-->
							<div class="check-overview">
								<a href="checklists/{{ $ipuc->checklistsID }}/edit"><div class="check-title">{{ $ipuc->checklistsname }}</div>
								<div class="check-details">
									<div class="check-user"><i class="fa fa-user icon-overview"></i> zugewiesen an: {{ $ipuc->username }}</div>
									<div class="check-date"><i class="fa fa-clock-o icon-overview"></i> zu erledigen bis: {{ date('d.m.Y', strtotime($ipuc->duedate)) }} um {{ date('G:i', strtotime($ipuc->reminder)) }} Uhr</div>
								</div>
								</a>
							</div>

							@endforeach
						</div>
					</div>

				</div>
			@endif
			
			@if(empty($closedUserChecklists))
			@else			
				<div class="col-lg-8 col-md-8 col-sm-12">

					<div class="panel panel-primary">
						<div class="panel-heading">
							<span>Erledigte Checklisten</span>
							<span class="pull-right clickable"><i class="glyphicon glyphicon-chevron-up"></i></span>
						</div>
						<div class="panel-body">
							@foreach($closedUserChecklists as $cuc)
								<!--<p>{{ $cuc->checklistsname }} (zugewiesen an: <strong>{{ $cuc->username }})</strong></p>-->
								<div class="check-overview">
									<a href="checklists/{{ $cuc->checklistsID }}/edit"><div class="check-title">{{ $cuc->checklistsname }}</div>
									<div class="check-details">
										<div class="check-user"><i class="fa fa-user icon-overview"></i> erhalten von: {{ $cuc->username }}</div>
									</div>
									</a>
								</div>
							@endforeach
						</div>
					</div>

				</div>
			@endif

		@endif
	</div>


</div>
@stop