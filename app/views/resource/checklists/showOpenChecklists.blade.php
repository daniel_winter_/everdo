@extends('layouts.layout')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
			<h1>Offene Checklisten</h1>
		</div>
	</div>

	<div class="row">
		@if(empty($openUserChecklists))
			<div class="col-lg-8 col-md-8 col-sm-12">
				<p class="alert alert-success" role="alert">Zur Zeit gibt es keine offenen Checklisten.</p>
			</div>
		@else

		<div class="col-lg-8 col-md-8 col-sm-12">

			<div class="panel panel-primary">
				<div class="panel-heading">
					<span>Offen</span>
					<span class="pull-right clickable"><i class="glyphicon glyphicon-chevron-up"></i></span>
				</div>
				<div class="panel-body">
					@foreach($openUserChecklists as $ouc)
						<!--<p>{{ $ouc->checklistsname }} (zugewiesen an: <strong>{{ $ouc->username }})</strong></p>-->

						<div class="check-overview">
							<a href="checklists/{{ $ouc->checklistsID }}/edit"><div class="check-title">{{ $ouc->checklistsname }}</div>
							<div class="check-details">
								<div class="check-user"><i class="fa fa-user icon-overview"></i> zugewiesen an: {{ $ouc->username }}</div>
								<div class="check-date"><i class="fa fa-clock-o icon-overview"></i> zu erledigen bis: {{ date('d.m.Y', strtotime($ouc->duedate)) }} um {{ date('G:i', strtotime($ouc->reminder)) }} Uhr</div>
							</div>
							</a>
						</div>

					@endforeach
				</div>
			</div>

		</div>

		@endif
	</div>

</div>
@stop