@extends('layouts.layout')
@section('content')
<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
	<div class="row">
		<div class="col-xs-12">
			<h1 id="checklistsname">Neuer Entwurf</h1>
		</div>
	</div>

	<div class="row">
		<div class="col-lg-12">
			
			<!--Alert-->
			@if ($errors->has())
			<div class="alert alert-danger" role="alert">
				@foreach ($errors->all() as $error)
				<p>{{ $error }}	</p>
				@endforeach
			</div>
			@endif
			
			<!--Tabs-->
			<ul class="nav nav-pills nav-justified" role="tablist">
				<li id="checklistsPattern" role="presentation" class="active"><a href="#">Entwurf</a></li>
				<li id="checklistsSettings" role="presentation" style="pointer-events:none;">
					<!--TODO on hover meldung-->
					<a id="checklistsSettingsLink" href="#" style="color:lightgrey;">Einstellungen</a></li>
			</ul>
			
			
			<!--Entwurf-->
			<div id="contentPattern">
				{{ Form::open(['route' => 'checklists.store', 'method' => 'POST'], array('class' => 'form-horizontal')) }}
					{{ Form::hidden('collectionsID', null)}}
					{{ Form::hidden('note', null)}}
				{{ Form::close() }}
				
				@include('includes.tasks')
			</div>
		

		<!--Einstellungen-->
		<div class="col-lg-12">
			<div id="contentSettings">
				{{ Form::open(['route' => 'assignments.store', 'method' => 'POST'], array('class' => 'form-horizontal')) }}
				
					{{ Form::hidden('checklistsID', null)}}
					<input type="hidden" name="checklistsname" id="hiddenChecklistsname" value="" />
				
				<div class="row">
					<div class="col-lg-6 col-md-6 col-sm-6">
						<div class="form-group group-settings1">
							<div class="settings-icon"><i class="fa fa-user icon-settings"></i><span class="settings-heading">Benutzer zuweisen: *</span></div>
							<div class="settings-function">{{ Form::select('userIDs[]', array('' => 'Benutzer zuweisen') + $getAllUsers, '', array('class' => 'form-control', 'required')) }}</div>
									@if(count($getAllUsers) <= 1)
									@else
										<div ng-controller="userController">
		  
									      <div class="btn btn-block btn-gray-small" ng-click="addUser()" style="cursor:pointer;"><span><i class="fa fa-plus icon-white"></i></span>Weitere Benutzer</div>
									      <div class="settings-function" ng-repeat="(key, value) in users" ng-if="$index < {{ $getNumberUsersWithoutAdmins }}-1">
									      	{{ Form::select('userIDs[]', array('' => 'Benutzer zuweisen') + $getAllUsers, '', array('class' => 'form-control', 'required')) }}

									      </div>
									    </div>
								    <!-- end angular part-->
									@endif
						</div>
					</div>

					<div class="col-lg-6 col-md-6 col-sm-6">
						<div class="form-group group-settings1">
							<div class="settings-icon"><i class="fa fa-clock-o icon-settings"></i><span class="settings-heading">Frist hinzufügen: *</span></div>
					        <div class='input-group datetime datepicker'>
							    {{ Form::text('duedate', null, array('class' => 'datepicker form-control', 'data-date-format' => 'DD.MM.YYYY', 'required')) }}
							    <!-- <input data-date-format="DD.MM.YYYY" type='text' class="form-control" /> -->
							    <span class="input-group-addon"><span class="fa fa-calendar icon-nav"></span>
							    </span>
							</div>
			
					        <div class="input-group datetime timepicker">
							    {{ Form::text('reminder', null, array('class' => 'timepicker form-control', 'data-format' => 'hh:mm:ss', 'required')) }}
							    <span class="input-group-addon"><span class="fa fa-clock-o icon-nav"></span>
							    </span>
					        </div>
							       
						</div>
					</div>

				<div class="second-row">
					<div class="col-lg-6 col-md-6 col-sm-6">
						<div class="form-group group-settings2">
							<div class="settings-icon"><i class="fa fa-comment icon-settings"></i><span class="settings-heading">Anmerkung hinzufügen: </span></div>
							<div class="settings-function">{{ Form::textarea('note', null, array('class' => 'form-control input-note', 'rows' => '1','placeholder' => 'Anmerkung zur Checkliste', 'style' => 'resize:none')) }}</div>
						</div>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-6">
						<div class="form-group group-settings2">
							<div class="settings-icon"><i class="fa fa-folder icon-settings"></i><span class="settings-heading">Sammlung zuweisen: </span></div>
							<div class="settings-function">{{ Form::select('collectionsID', array('' => 'Sammlung zuweisen') + $getAllCollections, '', array('class' => 'form-control')) }}</div>
						</div>
					</div>
				</div>
					 <!--<div class="col-lg-4 col-md-6 col-sm-6">
						<div class="form-group group-settings">
							<div class="settings-icon"><i class="fa fa-bell icon-settings"></i><span class="settings-heading">Reminder hinzufügen: </span></div>
					        <div class='input-group date' id='datetimepicker3'>
							    <input data-date-format="DD.MM.YYYY" type='text' class="form-control" />
							    <span class="input-group-addon"><span class="fa fa-calendar icon-nav"></span>
							    </span>
							</div> -->
			
					       <!-- <div class="input-group datetime" id="datetimepicker4">
					        	{{ Form::text('reminder', null, array('class' => 'form-control', 'data-format' => 'hh:mm:ss')) }}
							    
							    <span class="input-group-addon"><span class="fa fa-clock-o icon-nav"></span>
							    </span>
							</div>
						</div>
					</div>
						
					<div class="col-lg-4 col-md-6 col-sm-6">
						<div class="form-group group-settings">
							<div class="required-icon info-tooltip">
								<i class="fa fa-info-circle fa-2x icon-gray pull-right"></i>
							</div>
							<div class="settings-icon"><i class="fa fa-refresh icon-settings"></i><span class="settings-heading">Wiederholen?</span></div>
							<div class="settings-function">{{ Form::select('available', ['' => 'Wiederholen', '0' => 'nein', '1' => 'ja'], null) }}</div>
						</div>
					</div>-->

					<div class="form-group">
						<div class="col-md-12">
							<button type="submit" class="btn btn-default btn-gray" ng-click="saveChecklist(checkForm)"><i class="fa fa-share icon-white"></i>Entwurf absenden</a></button>
						</div>
					</div>
				</div>
			</div>
			</div>

			<!--Tooltip-->
			<script type="text/javascript">
				$(function() {
					$('.required-icon').tooltip({
						placement: 'left',
						title: 'Checkliste steht dem Nutzer dauerhaft zur Verfügung.'
					});
				});
			</script>
			{{ Form::close() }}
			
		</div>
	</div>
</div>
@stop