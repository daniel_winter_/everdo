@extends('layouts.layout')
@section('content')

<div class="container">
		<div class="row">
			<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
					<h1>Benutzer <span style="color: grey;">{{ $users->username }}</span> bearbeiten</h1>
			</div>
		</div>	

		<!-- Form Name -->
		<div class="row">
			 <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">

			@if ($errors->has())
				<div class="alert alert-danger" role="alert">
				@foreach ($errors->all() as $error)
					<p>{{ $error }}	</p>	
				@endforeach
				</div>
			@endif

			{{ Form::model($users, ['route' => ['users.update', $users->usersID], 'method' => 'PUT']) }}
				{{ Form::hidden('usersID', $users->usersID)}}
				{{ Form::hidden('username', $users->username)}}

			<!-- E-Mail input-->
			<div class="row">
				<div class="input-group">
					<span class="input-group-addon"><i class="fa fa-envelope"></i></span>
					<div>
						{{ Form::text('email', null, array('class' => 'form-control input-md', 'placeholder'=>'E-Mail')) }}
					</div>
				</div>
			</div>

			
			<!-- Select role -->
			<div class="row">
				<div class="input-group">
					<span class="input-group-addon"><i class="fa fa-star"></i></span>
					<div>
						{{ Form::select('rolesID', ['' => 'Bitte Rolle auswählen*', '1' => 'Administrator', '2' => 'User'], null, array('class' => 'btnUser form-control btn btn-default dropdown-toggle', 'data-toggle' => 'dropdown')) }}
					</div>
				</div>
			</div>


			<!-- Button -->
			<div class="row">
				<div class="form-group">
					<div class="col-md-12">
						<button type="submit" class="btn btn-default btn-gray"><i class="fa fa-check icon-white"></i>Speichern</a></button>
					</div>
				</div>
			</div>

		{{ Form::close() }}
	
@stop