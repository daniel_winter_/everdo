@extends('layouts.layout')
@section('content')

<div class="container">
		<div class="row">
			<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
					<h1>Alle Benutzer</h1>
			</div>
		</div>	

		<!-- Form Name -->
		<div class="row">
			 <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">

				@if( Session::has('successAddUser') )
					<p class="alert alert-success" role="alert"> {{ Session::get('successAddUser') }}</p>
				@endif

				@if( Session::has('successUpdateUser') )
					<p class="alert alert-success" role="alert"> {{ Session::get('successUpdateUser') }}</p>
				@endif

				@if( Session::has('successDeleteUser') )
					<p class="alert alert-success" role="alert"> {{ Session::get('successDeleteUser') }}</p>
				@endif
				
				<!-- New User Button -->
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						@if(empty($getAllUsers))
						<a href="users/create" class="btn btn-default btn-block btn-add">
							<span>
								<i class="fa fa-plus icon-add"></i></span>
								<p> Neuen Benutzer erstellen</p>
						</a>
						@else
							<a href="users/create" class="btn btn-default btn-block btn-gray">
							<span>
								<i class="fa fa-plus icon-white"></i>
							</span>
							Neuer Benutzer
							</a>

							<div class="row">
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
										<span class="settings-heading">
											<i class="fa fa-star icon-green"></i> Administrator</span>
								</div>
							</div>
						@endif
						
					</div>
				</div>

				<span class="help-block"></span>

				<div class="row">					
					@foreach($getAllUsers as $user)
						<div class="col-md-6">
							<div class="panel panel-primary panel-users">
								<a href="users/{{ $user->usersID }}/edit">
									<span>{{ $user->username }}</span>
									@if ($user->rolesID == 1)
				   					<i class="fa fa-star icon-green" data-toggle="tooltip" data-placement="right" title="Administrator"></i>
								    @endif
									<span>
										<!--{{ HTML::link('users/'.$user->usersID.'/edit', '', array('class' => 'glyphicon glyphicon-pencil pull-right', 'style' => 'position: relative; top: 4px; left: -35px;')) }}-->
										{{ Form::open(array('route' => array('users.destroy', $user->usersID), 'method' => 'delete', 'style' => 'float: right;', 'data-confirm' => 'Möchtest du den User wirklich löschen?')) }}
												{{Form::hidden('username', $user->username)}}
										      <button type="submit" href="{{ URL::route('users.destroy', $user->usersID) }}" class="btn-mini fa fa-trash pull-right btnTrash" style="border:none; background:none; float:right; top:2px; left:5px;"></button>
										{{ Form::close() }}
									</span>
								</a>
							</div>
						</div>
					@endforeach
				
				</div>
			</div>
		</div>
	</div>

@stop
