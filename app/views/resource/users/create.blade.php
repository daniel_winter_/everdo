@extends('layouts.layout')
@section('content')

<div class="container">

	<div class="row">
		<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
				<h1>Neuer Benutzer</h1>
		</div>
	</div>	

	<div class="row">
		<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">

			@if ($errors->has())
				<div class="alert alert-danger" role="alert">
					@foreach ($errors->all() as $error)
						<p>{{ $error }}	</p>	
					@endforeach
				</div>
			@endif
			{{ Form::open(['route' => 'users.store', 'method' => 'POST'], array('class' => 'form-horizontal')) }}
			
			<!-- Text input-->
			<div class="row">
				<div class="input-group">
					<span class="input-group-addon"><i class="fa fa-user"></i></span>
					<div>
						{{ Form::text('username', null, array('class' => 'form-control input-md', 'placeholder'=>'Benutzername')) }}
					</div>
				</div>
			</div>


			<!-- E-Mail input-->
			<div class="row">
				<div class="input-group">
					<span class="input-group-addon"><i class="fa fa-envelope"></i></span>
					<div>
						{{ Form::text('email', null, array('class' => 'form-control input-md', 'placeholder'=>'E-Mail Adresse')) }}
					</div>
				</div>
			</div>

			
			<!-- Select role -->
			<div class="row">
				<div class="input-group">
					<span class="input-group-addon"><i class="fa fa-star"></i></span>
					<div>
						{{ Form::select('rolesID', ['2' => 'Benutzer', '1' => 'Administrator'], null, array('class' => 'btnUser form-control btn btn-default dropdown-toggle', 'data-toggle' => 'dropdown')) }}
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="input-group">Hinweis: Es wird dem Benutzer ein generiertes Passwort zugesendet, das er jederzeit ändern kann.
				</div>
			</div>


			<!-- Button -->
			<div class="row">
				<div class="form-group">
					<div class="col-md-12">
						<button type="submit" class="btn btn-default btn-gray"><i class="fa fa-check icon-white"></i>Speichern</a></button>
					</div>
				</div>
			</div>

			{{ Form::close() }}

		</div>
	</div>	
</div>

	
														
					



@stop