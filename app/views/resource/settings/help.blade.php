@extends('layouts.layout')
@section('content')
<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
	<div class="row">
		<div class="col-md-12">
			<h1>Erste Schritte</h1>
		</div>
	</div>
	<style type="text/css">

	</style>
	<!-- tabs left -->
	<div class="tabbable tabs-left">
		<ul class="nav nav-tabs">
			<li class="active"><a href="#a" data-toggle="tab">Einführung </a></li>
			<li><a href="#b" data-toggle="tab">Registrierung und Anmeldung</a></li>
			<li><a href="#c" data-toggle="tab">Benutzer anlegen</a></li>
			<li><a href="#d" data-toggle="tab">Entwurf einer Checkliste erstellen</a></li>
			<li><a href="#e" data-toggle="tab">Einstellungen / Benutzer zuweisen</a></li>
			<li><a href="#f" data-toggle="tab">Sammlung anlegen/bearbeiten</a></li>
		</ul>
		<div class="tab-content">
			<div class="tab-pane active" id="a">
				<h3>Einführung</h3>
				<div class="js-video [vimeo, widescreen]">
					<iframe src="//player.vimeo.com/video/117952699?title=0&portrait=0&color=41ad78" width="700" height="250" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen title="0"></iframe>
				</div>
				<p>Widme diesem Leitfaden einige Minuten, um die grundlegenden Funktionen von Everdo kennenzulernen. Wir werden dir eine Reihe hilfreicher Everdo-Funktionen vorstellen, damit
				du dich leichter zurecht findest.
				</p>
				<p>Everdo ist ein großartiges Tool zur Verwaltung von Checklisten. Die Webapplikation ersetzt Papierchecklisten und ist speziell auf Vereine und gemeinnützige Organisationen abgestimmt. Besonders an der Applikation ist, dass du dich als Administrator registrieren und später deine Benutzer anlegen kannst, um diesen infolgedessen Aufgaben zuweisen zu können.
				<p>Die Checklisten werden üblicherweise am PC oder Laptop erstellt und der User hat später die Möglichkeit, seine Tasks bequem am Smartphone oder Tablet abzuarbeiten.</p>
			</div>
			
			<div class="tab-pane" id="b">
				<h3>Registrierung und Anmeldung</h3>
				Registriere dich als Administrator, um später deine Benutzer anlegen und ihnen Aufgaben zuweisen zu können. Du benötigst dafür einen Benutzernamen (mind. 5 Zeichen), deine E-Mail Adresse und ein geheimes Passwort.<p>
				<div class="js-video [vimeo, widescreen]"><img src="assets/images/help/registrierung.png"></img></div>
				<p>Aktiviere nun zur Bestätigung und Freigabe des Accounts den Link in der versandten everdo-Email.
					Melde dich mit deinem Benutzernamen und Passwort an, um zum Administratorbereich zugelangen. </div>
			<div class="tab-pane" id="c">
				<h3>Benutzer anlegen</h3>
				Lege dir unter dem Menüpunkt „Neuer Benutzer“ User an, um später deine Checklisten verteilen zu können. Du brauchst nur einen Benutzer
				namen und die E-Mail Adresse der bestimmten Personen und schon bekommen diese eine E-Mail mit deren Benutzerdaten, um
			sich anmelden zu können. Das Passwort wird vorerst automatisch generiert und kann später vom User jederzeit geändert werden. 
				<div class="js-video [vimeo, widescreen]"><img src="assets/images/help/neuer_benutzer.png"></img></div>
			</div>
			<div class="tab-pane" id="d">
				<h3>Entwurf einer Checkliste erstellen</h3>
				Klicke auf „Neuer Entwurf“ um eine Checkliste erstellen zu können. Benötigt wird nur ein Titel und schon kannst du loslegen und neue Aufgaben bzw. Checkpunkte hinzufügen. 
				<div class="js-video [vimeo, widescreen]"><img src="assets/images/help/neuer_entwurf.png"></img></div>
				Die Hierarchie der Checkpunkte kann mittels „Drag & Drop“ verändert werden. 
				Rücke die Punkte einfach etwas ein um einen Überpunkt zu erstellen, zu dem dann mehrere
				Unterpunkte ebenso mittels „Drag & Drop“ hinzugefügt werden können.<p> Die Reihenfolge der
				Aufgaben kann ebenso einfach verschoben werden, indem du die Checkpunkte nach oben oder unten ziehst.<p>
				<div class="js-video [vimeo, widescreen] png-checkpunkte"><img src="assets/images/help/checkpunkte.png"></img></div>
				Zur nachträglichen Bearbeitung der Checkpunkte musst du das Stift-Symbol ( <i class="fa fa-pencil icon-overview"></i>) verwenden.
				Zu den einzelnen Checkpunkten können mit dem Sprechblasen-Symbol ( <i class="fa fa-comment icon-overview"></i>) Notizen
				hinzugefügt werden und mit dem Mülleimer-Symbol ( <i class="fa fa-trash icon-overview"></i>) kann der Checkpunkt komplett gelöscht werden.<p>
				<p>Wenn du fertig bist, speichere den Entwurf um weitere Einstellungen vornehmen zu können und die Checkliste an bestimmte Benutzer zuweisen zu können.</div>
			<div class="tab-pane" id="e">
				<h3>Einstellungen / Benutzer zuweisen</h3>
				Sobald du alle Checkpunkte angelegt und gespeichert hast, kannst du die Einstellungen zu der Checkliste vornehmen und sie einem Benutzer zuweisen. Klicke dazu auf den „Einstellungen“ Tab. <p> 
				<div class="js-video [vimeo, widescreen]"><img src="assets/images/help/einstellungen1.png"></img></div>
				<p>Im ersten Feld musst du einen oder mehrere Benutzer, die später die Checkliste abarbeiten sollen, auswählen. Setze danach im nächsten Feld eine Frist mit Datum und Uhrzeit, bis wann die Checkliste erledigt werden soll (Pflichtfelder). Der Benutzer erhält zudem eine E-Mail wenn ihm eine Checkliste zugewiesen wurde damit er keine übersehen kann.<p>
				<p>Außerdem ist es möglich, eine Anmerkung als Hinweis zu der Checkliste hinzuzufügen, die der Benutzer dann bei der jeweiligen Checkliste sieht und zusätzlich kannst du den Checklistenentwurf noch in eine bereits erstellte Sammlung geben, um eine bessere Übersicht über deine Entwürfe zu bewahren.</p>
				<div class="js-video [vimeo, widescreen]"><img src="assets/images/help/einstellungen2.png"></img></div>
			</div>
			<div class="tab-pane" id="f">
				<h3>Sammlung anlegen/bearbeiten</h3>
				Um eine bessere Übersicht über deine Checklistenentwürfe zu bewahren, kannst du im Navigationspunkt „Neue Sammlung“ eine Sammlung erstellen und Entwürfe zuweisen indem du die Checkboxen der gewünschten Listen anklickst und somit abhakst. <p> 
				<p>Du siehst die Sammlungen dann bei der Entwurfsübersicht unter „Alle Entwürfe“.</p>
				<div class="js-video [vimeo, widescreen]"><img src="assets/images/help/neue_sammlung.png"></img></div>
			</div>
		</div>
	</div>
	
</div>
@stop