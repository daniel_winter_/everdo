@extends('layouts.layout')
@section('content')

	<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
		<div class="row">
			<div class="col-md-12">
				<h1>Einstellungen</h1>
			</div>
		</div>

		<!--<p>Passen Sie Ihren Account an, indem Sie Ihr persönliches Logo hochladen.</p>-->

		@foreach ($errors->all() as $error)
			<p class="alert alert-danger" role="alert">{{ $error }}	</p>	
		@endforeach

		@if( Session::has('successAddSettingsLogo') )
			<p class="alert alert-success" role="alert"> Logo erfolgreich geändert.</p>
		@endif
		

		<h4>Eigenes Logo hochladen</h4>
		{{ Form::open(['route' => 'settings.store', 'method' => 'POST', 'files' => true], array('class' => 'form-horizontal')) }}

		<div class="row">
			<div class="form-group">	
				{{ Form::file('image', ['class' => 'btn btn-m', 'enctype' => 'multipart/form-data', 'accept' => 'image/*']) }}
			</div>
		</div>

		<!--<h3>Farbe anpassen</h3>

		<div class="row">
			<div class="form-group col-md-4">	
				{{ Form::input('color', 'color', '#41ad78', array('class' => 'input-big')) }}
			</div>
		</div>-->
		
		<div class="form-group pull-right">
			{{ Form::submit('Speichern', array('class' => 'btn btn-lg btn-block btn-gray')) }}
		{{ Form::close() }}
		</div>

	</div>

@stop