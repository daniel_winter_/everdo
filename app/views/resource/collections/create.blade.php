@extends('layouts.layout')
@section('content')

<div class="container">
	<div class="row">
		<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
				<h1 id="collectionsname">Neue Sammlung</h1>
		</div>
	</div>	
	
	<div class="row">
		@if(count($getAllChecklistsNames) < 2)
			<div class="col-lg-8 col-md-8 col-sm-6">
				<p class="alert alert-success" role="alert">Sie müssen mindestens 2 Entwürfe erstellen, um eine Sammlung anlegen zu können.</p>
			</div>
		@else
			<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
				{{ Form::open(['route' => 'collections.store', 'method' => 'POST'], array('class' => 'form-horizontal')) }}
			
				@if ($errors->has())
					<div class="alert alert-danger" role="alert">
					@foreach ($errors->all() as $error)
						<p>{{ $error }}	</p>	
					@endforeach
					</div>
				@endif

				<fieldset>
					<div class="row">
						<div class="form-group col-md-12">	
							{{ Form::text('collectionsname', null, array('id' => 'getCollectionsName', 'class' => 'form-control input-lg', 'placeholder'=>'Titel der Sammlung *', 'maxlength' => 30)) }}
						</div>
					</div>
					<p class="settings-heading">Bitte Checklisten auswählen:</p>
					<div class="row form-group">
							<ul>
								@foreach ($getAllChecklistsNames as $checklists)
								<div class="col-lg-6">
									<li class="panel panel-primary panel-users">
									<input class="checkbox-check" type="checkbox" tabindex="1" name="checklists[{{$checklists}}]" id="{{$checklists}}"> 
									<label class="label-checklists" for="{{$checklists}}">{{$checklists}}</label>
									</li>
								</div>
								@endforeach
<!-- 								@foreach ($getAllChecklistsIDs as $ids)
									<input type="hidden" tabindex="1" name="checklists[{{$ids}}]" id="{{$checklists}}">
								@endforeach -->
							</ul>
					</div>

					<div class="row">
						<div class="form-group">
							<div class="col-md-12">
								<button type="submit" class="btn btn-default btn-gray"><i class="fa fa-check icon-white"></i>Speichern</a></button>
							</div>
						</div>
					</div>
				</fieldset>

				{{ Form::close() }}		
			</div>
		@endif

	</div>
</div>
@stop