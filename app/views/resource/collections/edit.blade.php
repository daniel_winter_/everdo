@extends('layouts.layout')
@section('content')

	<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
		<div class="row">
			<div class="col-xs-12">
				<h1>Sammlung <span style="color: grey;">{{ $collections[0]->collectionsname}}</span> bearbeiten</h1>
			</div>
		</div>

		@if ($errors->has())
			<div class="alert alert-danger" role="alert">
			@foreach ($errors->all() as $error)
				<p>{{ $error }}	</p>	
			@endforeach
			</div>
		@endif
		
		<div class="row">
			<div class="col-lg-12">

				{{ Form::model($collections, array('route' => array('collections.update', $collections[0]->collectionsID), 'files' => true, 'method' => 'PUT')) }}

					{{ Form::hidden('collectionsID', $collections[0]->collectionsID)}}

					<!-- Form Name -->
					<div class="row">
						<div class="form-group col-md-12">	
						{{ Form::text('collectionsname', $collections[0]->collectionsname, array('class' => 'form-control input-lg', 'placeholder'=>'Titel der Sammlung *')) }}
						</div>

					</div>

					<div class="row">
						<div class="col-md-12">
							<p class="settings-heading">Bitte Checklisten aus/abwählen:</p>
							<div class="row form-group">
							<ul>
								@foreach ($getCollectionsWithChecklists as $gcwc)
								<div class="col-lg-6">
									<li class="panel panel-primary panel-users">
										<input class="checkbox-check" type="checkbox" tabindex="{{$gcwc->checklistsID}}" checked="checked" name="checklists[{{ $gcwc->checklistsID}}]" id="{{$gcwc->checklistsID}}"> 
										<label class="label-checklists" for="{{ $gcwc->checklistsID}}">{{ $gcwc->checklistsname}}</label>
									</li>
								</div>
								@endforeach
								@foreach ($getAllChecklists as $checklists)
								<div class="col-lg-6">
									<li class="panel panel-primary panel-users">
										<input class="checkbox-check" type="checkbox" tabindex="{{$checklists->checklistsID}}" name="checklists[{{$checklists->checklistsID}}]" id="{{$checklists->checklistsID}}"> 
										<label class"label-checklists" for="{{$checklists->checklistsID}}">{{$checklists->checklistsname}}</label>
									</li>
								</div>
								@endforeach
							</ul>
						</div>
						</div>
					</div>

					<div class="row">
						<div class="form-group">
							<div class="col-md-6 collection-save">
								{{ Form::submit('Sammlung speichern', array('class' => 'btn btn-default btn-block btn-gray')) }}
								{{ Form::close() }}
							</div>
							<div class="col-md-6">
								{{ Form::open(array('route' => array('collections.destroy', $collections[0]->collectionsID), 'method' => 'delete', 'data-confirm' => 'Möchtest du diese Sammlung wirklich löschen?')) }}
								{{ Form::hidden('collectionsID', $collections[0]->collectionsID) }}
									<button type="submit" href="{{ URL::route('collections.destroy', $collections[0]->collectionsID) }}" class="btn btn-default btn-block btn-gray"><i class="fa fa-trash icon-white"></i>Löschen</button>
								{{ Form::close() }}
							</div>
						</div>
					</div>	
			</div>
		</div>
	</div>
@stop