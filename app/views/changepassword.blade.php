@extends('layouts.layout')
@section('content')

<div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                    <h1>Passwort ändern</h1>
            </div>
        </div>  

        <!-- Form Name -->
        <div class="row">
             <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
            
            @if( Session::has('successUpdatePassword') )
                    <p class="alert alert-success" role="alert"> {{ Session::get('successUpdatePassword') }}</p>
            @endif
            @if ($errors->has())
                <div class="alert alert-danger" role="alert">
                @foreach ($errors->all() as $error)
                    <p>{{ $error }} </p>    
                @endforeach
                </div>
            @endif

            {{ Form::open(array('route' => array('updatepassword'), 'method' => 'post')) }}

            <!-- Altes Passwort input-->
            <h4>Altes Passwort</h4>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                    <div>
                        {{ Form::password('password_old',array('class' => 'form-control input-md','placeholder' => 'Altes Passwort')) }}
                    </div>
                </div>
            </div>

            
            <!-- Neues Passwort -->
            <h4>Neues Passwort</h4>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                    <div>
                        {{ Form::password('password_new',array('class' => 'form-control input-md','placeholder' => 'Neues Passwort')) }}
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                    <div>
                    {{ Form::password('password_confirmation',array('class' => 'form-control input-md','placeholder' => 'Neues Passwort bestätigen')) }}
                    </div>
                </div>
            </div>


            <!-- Button -->
            <div class="row">
                <div class="form-group">
                    <div class="col-md-12 login-button">
                        <button type="submit" class="btn btn-default btn-gray"><i class="fa fa-check icon-white"></i>Speichern</button>
                    </div>
                </div>
            </div>

        {{ Form::close() }}
    
@stop