<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <h2 style="color:#41AD78;">Neue Checkliste vorhanden</h2>

        <div>
            <p>Hallo {{ $name }},</p>
            <p>Ihnen wurde eine neue Checkliste ({{ $detail }}) in Ihrem everdo Account zugewiesen. Diese steht Ihnen nun zur Verfügung.</p>
            <p><a style="color:#41AD78;" href="{{ URL::to('login') }}">Zum Loginbereich</a></p>
            <div>
                <img src="http://flock-0678.students.fhstp.ac.at/mail/default.svg" alt="everdo Logo" width="100" height="150">
            </div>

            <p>Viel Spaß mit everdo.</p>

            <p>Ihr everdo Team</p>
            
        </div>

    </body>
</html>