<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <h2 style="color:#41AD78;">Bestätigung Ihrer E-Mail Adresse</h2>

        <div>
            <p>Hallo {{ $username }},</p>
            <p>vielen Dank das Sie einen Account bei everdo erstellt haben.
            Bevor Sie loslegen können, müssen Sie noch den Link bestätigen um ihren Account zu aktivieren.</p>
            <p><a style="color:#41AD78;" href="{{ URL::to('register/verify/' . $confirmation_code) }}">Aktiviere den Account</a>.</p>
            <div>
                <img src="http://flock-0678.students.fhstp.ac.at/mail/default.svg" alt="everdo Logo" width="100" height="150">
            </div>

            <p>Vielen Dank und viel Spaß mit everdo.</p>

            <p>Ihr everdo Team</p>
            
        </div>

    </body>
</html>