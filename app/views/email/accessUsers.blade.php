<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <h2 style="color:#41AD78;">Zugangsdaten für Ihren Account bei everdo</h2>

        <div>
            <p>Hallo {{ $username }},</p>
            <p>mit folgenden Daten können Sie sich bei everdo anmelden:</p>
            <p>Username: {{ $username }}</p>
            <p>Passwort: {{ $password }}</p>
            <p>E-Mail: {{ $email }}</p>
            <p><a style="color:#41AD78;" href="{{ URL::to('login') }}">Zum Loginbereich</a></p>
            <div>
                <img src="http://flock-0678.students.fhstp.ac.at/mail/default.svg" alt="everdo Logo" width="100" height="150">
            </div>

            <p>Viel Spaß mit everdo.</p>

            <p>Ihr everdo Team</p>
            
        </div>

    </body>
</html>