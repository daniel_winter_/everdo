<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChecklistsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('checklists', function(Blueprint $table)
		{
			$table->increments('checklistsID');
			$table->integer('usersID')->unsigned();
			$table->foreign('usersID')->references('usersID')->on('users')->onDelete('cascade')->onUpdate('cascade');
			$table->integer('collectionsID')->nullable()->unsigned();
			$table->foreign('collectionsID')->references('collectionsID')->on('collections')->onDelete('set null')->onUpdate('cascade');
			$table->string('checklistsname');
			$table->boolean('available')->nullable();
			$table->string('availableproperty')->nullable();
			$table->string('availablevalue')->nullable();
			$table->string('note')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('checklists');
	}

}

