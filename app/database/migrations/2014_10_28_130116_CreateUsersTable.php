<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->increments('usersID');
			$table->integer('rolesID')->unsigned();
			$table->foreign('rolesID')->references('rolesID')->on('roles')->onDelete('cascade')->onUpdate('cascade');
			$table->integer('settingsID')->unsigned();
			$table->foreign('settingsID')->references('settingsID')->on('settings')->onDelete('cascade')->onUpdate('cascade');
			$table->string('username');
			$table->string('password');
			$table->string('email');
			$table->boolean('confirmed')->default(0);
            $table->string('confirmation_code')->nullable();
			$table->timestamp('lastlogin');
			$table->string('remember_token')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
