<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssignmentsUsersChecklistsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('assignmentsuserschecklists', function(Blueprint $table)
		{
			$table->increments('aucID');
			$table->integer('checklistsID')->unsigned();
			$table->foreign('checklistsID')->references('checklistsID')->on('checklists')->onDelete('cascade')->onUpdate('cascade');
			$table->integer('usersID')->unsigned();
			$table->foreign('usersID')->references('usersID')->on('users')->onDelete('cascade')->onUpdate('cascade');
			$table->integer('checklistsstatusID')->unsigned();
			$table->foreign('checklistsstatusID')->references('checklistsstatusID')->on('checklistsstatus')->onDelete('cascade')->onUpdate('cascade');
			$table->timestamp('duedate');
			$table->time('reminder');
			$table->string('note')->nullable();
			$table->string('noteFromUser')->nullable();
			$table->boolean('sent')->default(0);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('assignmentsuserschecklists');
	}

}
