<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTasks extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tasks', function(Blueprint $table)
		{
			$table->increments('tasksID');
			$table->integer('checklistsID')->unsigned();
			$table->foreign('checklistsID')->references('checklistsID')->on('checklists')->onDelete('cascade')->onUpdate('cascade');
			$table->string('tasksname');
			$table->string('tasksnote')->nullable();
			$table->integer('order');
			$table->integer('parentID');
			$table->boolean('checked')->default(0);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tasks');
	}

}
