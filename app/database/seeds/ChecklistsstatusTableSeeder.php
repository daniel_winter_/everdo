<?php
// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class ChecklistsstatusTableSeeder extends Seeder {

	public function run()
	{
        DB::table('checklistsstatus');

        $checklistsstatus = [
            [
                'statusname' => 'offen'
            ],
            [
                'statusname' => 'in Bearbeitung'
            ],
            [
                'statusname' => 'erledigt'
            ]
        ];

        foreach($checklistsstatus as $checklistsstati){
            Checklistsstatus::create($checklistsstati);
        }
	}

}