<?php
// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class SettingsTableSeeder extends Seeder {

	public function run()
	{
		DB::table('settings');

		$setts = [
            [
                'settingslogo' => '/path',
                'settingscolor' => '#4387FC'
            ],
            [
                'settingslogo' => '/path2',
                'settingscolor' => '#4787FQ'
            ]
        ];

        foreach($setts as $setti){
            Settings::create($setti);
        }
	}

}