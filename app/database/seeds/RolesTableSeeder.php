<?php
// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class RolesTableSeeder extends Seeder {

	public function run()
	{
		DB::table('roles');

		$roles = [
            [
                'rolesname' => 'Admin',
                'rolesdescription' => 'Administrator'
            ],
            [
                'rolesname' => 'User',
                'rolesdescription' => 'Benutzer'
            ]
        ];

        foreach($roles as $role){
            Roles::create($role);
        }
	}

}