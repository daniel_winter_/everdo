<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		$this->call('SettingsTableSeeder');
		$this->call('RolesTableSeeder');
		$this->call('UsersTableSeeder');
		$this->call('CollectionsTableSeeder');
		$this->call('ChecklistsstatusTableSeeder');
		$this->call('ChecklistsTableSeeder');
		$this->call('AssignmentsUsersChecklistsTableSeeder');
		$this->call('TasksTableSeeder');
	}

}
