<?php

class Tasks extends Eloquent {

	protected $table = 'tasks';
	protected $primaryKey = 'tasksID';
	protected $fillable = [];
	protected $guarded = [];
}