<?php

class Assignmentsuserschecklists extends Eloquent {

	protected $table = 'assignmentsuserschecklists';
	protected $primaryKey = 'aucID';
	protected $fillable = [];
	protected $guarded = [];


	public static $rules = [
	     'store' => [


	     ]
     ];

     public static $messages  = [
		'store' => [
		 	'userIDs.required' 		=> 'Bitte geben Sie mindestens einen Benutzer an, der die Checkliste bearbeiten soll.',
			'duedate.required' 		=> 'Bitte geben Sie ein Datum an, bis wann die Checkliste fertig bearbeitet werden soll.',
			'reminder.required' 	=> 'Bitte geben Sie eine Uhrzeit an, bis wann die Checkliste fertig bearbeitet werden soll.'
	     ]
     ];

}