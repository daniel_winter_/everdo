<?php

class Settings extends Eloquent {

	protected $table = 'settings';
	protected $primaryKey = 'settingsID';
	protected $fillable = [];
	
	public static $rules = [
		'storeimage' => [
			'image'			=> 'required'
	     ],
		'store' => [
			'image' => 'mimes:jpeg,jpg,png,gif,svg|max:3072'
	     ]
     ];

	public static $messages  = [
	     'storeimage' => [
		 	'required' 				=> 'Sie müssen schon ein Bild hochladen.'
			
	     ],
		 'store' => [
			'image.mimes'			=> 'Das Bild muss vom Typ jpeg, jpg, png, svg oder gif sein.',
			'image.max'				=> 'Das Bild darf nicht größer als 3 MB sein.'
	     ]
     ];
	
}