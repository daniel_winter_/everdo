<?php

class Checklists extends Eloquent {

	protected $table = 'checklists';
	protected $primaryKey = 'checklistsID';
	protected $fillable = [];
	protected $guarded = [];

	public function collections() {
        return $this->hasOne('Collections');
    }

    public static $rules = [
	     'create' => [
			'checklistsname'		=> 'required|min:5'

	     ],

	     'update' => [
			'checklistsID'			=> 'required'
	     ]
     ];

     public static $messages  = [
		'create' => [
		 	'min' 							=> 'Der Title der Checkliste muss mindestens :min Zeichen haben.',
		 	'checklistsname.required' 		=> 'Bitte geben Sie einen Titel für die Checkliste an.'
			
	     ],
		 'update' => [
		 	'required' 				=> 'Bitte geben die eine ID an.'
	     ]
     ];
	
}