<?php

class Users extends Eloquent{

	// use UserTrait, RemindableTrait;
	protected $table = 'users';

	protected $hidden = array('password','remember_token');

	protected $primaryKey = 'usersID';

	protected $fillable = ['username','email','password'];

	public static $rules = [
		'login' => [
			'username'					=> 'required|exists:users,username|regex:/^[A-Za-z1-9][A-Za-z0-9_-]*$/',
			'password'					=> 'required|min:5'
	     ],
	    'update' => [
			'email'						=> 'required|email',
			'rolesID'					=> 'required'
	     ],
	    'edit'   => [
			'username'					=> 'required|min:5|unique:users|regex:/^[A-Za-z1-9][A-Za-z0-9_-]*$/',
			'email'						=> 'required|email|unique:users',
			'rolesID'					=> 'required'
	    ],
	    'register' => [ 
	        'username' 					=> 'min:5|required|unique:users,username|regex:/^[A-Za-z1-9][A-Za-z0-9_-]*$/',
            'email' 					=> 'required|unique:users,email',
			'password' 					=> 'required|min:5|confirmed|regex:/^[A-Za-z1-9][A-Za-z0-9_-]*$/',
            'password_confirmation'		=> 'required|min:5'
	     ],
	     'forgetpassword' => [
	     	'email'						=> 'required|email|exists:users'
	     ],
	     'updatepassword' => [
            'password_old' 				=> 'required',
            'password_new'				=> 'required|min:5|regex:/^[A-Za-z1-9][A-Za-z0-9_-]*$/',
            'password_confirmation'		=> 'required|same:password_new'
	     ]
     ];

	public static $messages  = [
		'login' => [
			'username.exists'			=> 'Der eingegebene Benutzername existiert nicht.',
			'username.required'			=> 'Bitte geben Sie einen Benutzername an.',
			'password.required'			=> 'Bitte geben Sie ein Passwort an.',
			'password.min'				=> 'Das eingegebene Passwort muss mindestens :min Zeichen haben.',
			'username.regex'			=> 'Bitte geben Sie ein gültiges Format an.'
	     ],
	    'register' => [
			'username.required'			=> 'Bitte geben Sie einen Benutzername an.',
			'username.unique'			=> 'Der eingegebene Benutzername ist bereits vorhanden.',
			'email.required'			=> 'Bitte geben Sie eine E-Mail Adresse an.',
			'email.unique'				=> 'Die eingegebene E-Mail Adresse ist bereits vorhanden.',
			'password.required'			=> 'Bitte geben Sie ein Passwort an.',
			'password.confirmed' 		=> 'Die eingebene Passwortbestätigung stimmt nicht mit dem Passwort überein.',
            'password_confirmation.required'=> 'Bitte geben Sie eine Passwortbestätigung an.',
			'username.regex'			=> 'Bitte geben Sie ein gültiges Format an.',
			'password.regex'			=> 'Bitte geben Sie ein gültiges Format an.'
	     ],
	    'update' => [
	        'required' 				=> 'Dieses Feld ist Pflicht.',
			'min' 					=> 'Der eingegebene Benutzername muss mindestens :min Zeichen haben.',
			'email' 				=> 'Die eingegebene E-Mail Adresse ist nicht gültig.',
			'username.required' 	=> 'Bitte geben Sie einen Benutzername an.',
			'email.required' 		=> 'Bitte geben Sie eine E-Mail Adresse an.',
			'rolesID.required' 		=> 'Bitte geben Sie eine Rolle an.'
	        ],
	    'edit'   => [
	        'required' 				=> 'Dieses Feld ist Pflicht.',
			'min' 					=> 'Der eingegebene Benutzername muss mindestens :min Zeichen haben.',
			'email' 				=> 'Die eingegebene E-Mail Adresse ist nicht gültig.',
			'email.unique'			=> 'Diese E-Mail gibt es bereits.',
			'username.required' 	=> 'Bitte geben Sie einen Benutzername an.',
			'email.required' 		=> 'Bitte geben Sie eine E-Mail Adresse an.',
			'rolesID.required' 		=> 'Bitte geben Sie eine Rolle an.',
			'username.unique'		=> 'Diesen Benutzernamen gibt es bereits.',
			'username.regex'		=> 'Bitte geben Sie ein gültiges Format an.'
	        ],
 	    'forgetpassword'   => [
			'required'				=> 'Bitte geben Sie eine E-Mail Adresse an.',
			'email'					=> 'Bitte geben Sie eine gültige E-Mail Adresse an.',
			'email.exists'			=> 'Die eingegebene E-Mail Adresse ist nicht gültig.'
	        ],
 	    'updatepassword'   => [
			'password_old.required'				=> 'Bitte geben Sie Ihr altes Passwort an.',
			'password_new.required'				=> 'Bitte geben Sie ein neues Passwort an.',
			'password_confirmation.required'	=> 'Bitte geben Sie eine Passwortbestätigung ein.',
			'password_new.min'					=> 'Das neue Passwort muss mindestens :min Zeichen haben.',
			'password_new.regex'				=> 'Bitte geben Sie ein gültiges Format an.',
			'password_confirmation.min'			=> 'Die Passwort-Bestätigung muss mindestens :min Zeichen haben.',
			'password_confirmation.same'		=> 'Die beiden Passwörter stimmen nicht überein.'


	        ]
     ];
		
	

	// protected $hidden = array('password', 'remember_token');

	public function settings()
    {
        return $this->hasOne('Settings');
    }
	
	
}
