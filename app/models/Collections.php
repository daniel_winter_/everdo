<?php

class Collections extends Eloquent {
	protected $table = 'collections';
	protected $primaryKey = 'collectionsID';
	protected $fillable = [];

	public static $rules = [
		'create' => [
			'collectionsname'			=> 'required|min:5',
			'checklists'				=> 'required|min:2'
	     ],
	     'update' => [
			'collectionsname'			=> 'required|min:5',
			'checklists'				=> 'required|min:2'
	     ]

     ];

     public static $messages  = [
		 'create' => [
		 	'min' 							=> 'Der Sammlungsname muss mindestens :min Zeichen haben.',
		 	'collectionsname.required' 		=> 'Bitte geben Sie einen Sammlungsnamen an.',
		 	'checklists.required' 			=> 'Bitte wählen Sie mindestens zwei Checklisten aus.',
		 	'checklists.min' 				=> 'Bitte wählen Sie mindestens :min Checklisten aus.'
	     ],
	     'update' => [
		 	'collectionsname.required' 		=> 'Bitte geben Sie einen Sammlungsnamen an.',
		 	'collectionsname.min' 			=> 'Der Sammlungsname muss mindestens :min Zeichen haben.',
		 	'checklists.required' 			=> 'Bitte wählen Sie mindestens zwei Checklisten aus.',
		 	'checklists.min' 				=> 'Bitte wählen Sie mindestens :min Checklisten aus.'
	     ]
     ];

	public function checklists() {
        return $this->hasMany('Checklists');
    }
}