<?php

class CollectionsController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /collections
	 *
	 * @return Response
	 */

	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /collections/create
	 *
	 * @return Response
	 */
	public function create()
	{
		$usersID = Auth::id();

		$getAllChecklists = DB::table('checklists')
			->select('checklistsID', 'checklistsname')
			->where ('usersID', '=', $usersID)
			->groupBy('checklistsname') //distinct
			->get();

		$output = array_map(function ($object) { return $object->checklistsname; }, $getAllChecklists);
		$output2 = array_map(function ($object) { return $object->checklistsID; }, $getAllChecklists);

		// return $output2;

		return View::make('resource.collections.create', ['getAllChecklistsNames' => $output, 'getAllChecklistsIDs' => $output2]);
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /collectionsfolder
	 *
	 * @return Response
	 */
	public function store()
	{
		$usersID = Auth::id();
		$input = Input::all();
		

		$collections = new Collections();

		$checklistsChecked = Input::get('checklists');

		$validator = Validator::make( $input, Collections::$rules['create'], Collections::$messages['create'] );

		if ($validator->fails())
	    {
	            return Redirect::to('collections/create')
	            	->withErrors($validator)
	            	->withInput();
	    }
	    else {

				$id = DB::table('collections')->insertGetId(
	                array(
	                	'collectionsname'	=> $input['collectionsname'],
	                )
	            );

	            

	            foreach(array_keys($checklistsChecked) as $key) {

			        // $checklists = Checklists::firstOrCreate(array('checklistsname' => $checklists->checklistsname=$key, 'usersID' => $usersID));

	 				$checklists = new Checklists();
	 				$checklists->usersID=$usersID;
	 				$checklists->collectionsID=$id;
	 				$checklists->checklistsname=$key;
	 				$checklists->save();
				}

	            return Redirect::to('checklists')
	            	->with('successAddCollection', 'Sammlung <strong>'.$input['collectionsname'].'</strong> erfolgreich angelegt.');

        }
	}

	/**
	 * Display the specified resource.
	 * GET /collections/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /collections/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{

		$usersID = Auth::id();

		$collections = DB::table('checklists as cl')
			->leftjoin('collections as col', 'cl.collectionsID', '=', 'col.collectionsID')
			->select('cl.collectionsID', 'col.collectionsname')
			->where ('cl.usersID', '=', $usersID)
			->where ('col.collectionsID', '=', $id)
			->whereNotNull('cl.collectionsID')
			->groupBy('cl.collectionsID')
		    ->get();

		$getAllChecklists = DB::table('checklists as cl')
			->leftjoin('collections as col', 'cl.collectionsID', '=', 'col.collectionsID')
			->select('cl.checklistsID', 'cl.checklistsname', 'col.collectionsID', 'col.collectionsname')
			->where ('cl.usersID', '=', $usersID)
			->whereNull ('col.collectionsID')
			->groupBy('cl.checklistsname')
		    ->get();

		// alle Collections mit der ID für die Editierung
		$getCollectionsWithChecklists = DB::table('checklists')
			->leftjoin('collections', 'checklists.collectionsID', '=', 'collections.collectionsID')
			->select('checklists.checklistsID', 'checklists.collectionsID', 'checklists.checklistsname', 'collections.collectionsname')
			->where ('checklists.usersID', '=', $usersID)
			->where ('collections.collectionsID', '=', $id)
			// ->whereNotNull('checklists.collectionsID')
			->groupBy('checklists.checklistsname')
		    ->get();

		// return $getAllChecklists;
		return View::make('resource.collections.edit')->with(array('getAllChecklists' => $getAllChecklists, 'collections' => $collections, 'getCollectionsWithChecklists' => $getCollectionsWithChecklists));
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /collections/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$usersID = Auth::id();

		$input = Input::all();
		
		// return $input;
		$checklistsChecked = Input::get('checklists', false);

		$collection = new Collections();

		$validator = Validator::make($input, Collections::$rules['update'], Collections::$messages['update']);

		if ( $validator->fails() ) {

	        return Redirect::to('collections/'.$id.'/edit')
	        	->withErrors($validator)
	        	->withInput();		
		}

		else {
			$date = new DateTime;
			//first change collection name
			DB::table('collections')
				->where('collectionsID', $input['collectionsID'])
                ->update(
                    array(
                    	'collectionsname'	=> $input['collectionsname'],
                        'updated_at'    	=> $date
                )
            );

            //then set all collectionsIDs to null in the collection
            DB::table('checklists')
				->where('collectionsID', $input['collectionsID'])
                ->update(
                    array(
                    	'collectionsID'	=> null                )
            );

            //set the collectionID new to the checklists
			foreach($checklistsChecked as $key => $value) {
				// return $key;
				DB::table('checklists')
					->where('checklistsID', $key)
				    ->update(
				        array(
				        	'collectionsID'	=> $input['collectionsID']                )
				);
			}

            return Redirect::to('checklists')->with('successUpdateCollection', 1);
			
		}
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /collections/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		DB::table('collections')
            ->where('collectionsID', '=', $id)
            ->delete();

        return Redirect::to('checklists')->with('successDeleteCollection', 1);
	}

}