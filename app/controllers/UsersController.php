<?php

class UsersController extends BaseController {


	public function changepassword()
	{
        $usersID = Auth::id();
		return View::make('changepassword');
	}

	public function updatepassword() {

		$data = Input::only(['password_old','password_new','password_confirmation']);
		//return $data;
		$user = Auth::user();

		$validator = Validator::make( $data, Users::$rules['updatepassword'], Users::$messages['updatepassword']);

		if ($validator->fails())
	    {
	        return Redirect::to('changepassword')
	        	->withErrors($validator)
	           	->withInput();
	    }
	    else {

	    	if (!Hash::check(Input::get('password_old'), $user->password)) {
            	return Redirect::action('changepassword')->withErrors('Altes Passwort stimmt nicht überein.');
        	}else{
            	$user->password = Hash::make(Input::get('password_new'));
            	$user->save();
            	return Redirect::route('changepassword')->with('successUpdatePassword', 'Passwort erfolgreich geändert.');
            }
	    }
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        //Die User ID
        $usersID = Auth::id();
        //Herausfinden der settingsID
        $settingsID = DB::table('users')->where('usersID', $usersID)->pluck('settingsID');

        $getAllUsers = DB::table('users')
            ->select('usersID', 'username', 'rolesID')
            ->where('settingsID', $settingsID)
            ->whereNotIn('usersID', array($usersID))
            ->get();

         // return $getAllUsers;
		return View::make('resource.users.index', ['getAllUsers' => $getAllUsers]);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{

		return View::make('resource.users.create');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$usersID = Auth::id();
		$settingsID = DB::table('users')->where('usersID', $usersID)->pluck('settingsID');

		$input = Input::all();
		$users = new User();

		$validator = Validator::make( $input, Users::$rules['edit'], Users::$messages['edit'] );

		if ($validator->fails())
	    {
	            return Redirect::to('users/create')
	            	->withErrors($validator)
	            	->withInput();
	    }
	    else {
	    		$date = new DateTime;
				$user = new User;
	        	$user->username = Input::get('username');
	    		$user->email = Input::get('email');
	    		$password = str_random(8);

				$id = DB::table('users')->insertGetId(
	                array(
	                	'rolesID'		=> $input['rolesID'],
	                	'settingsID'	=> $settingsID,
	                    'username'    	=> $user->username,
	                    'password'    	=> Hash::make($password),
	                    'email'  		=> $user->email,
	                    'confirmed'		=> 1,
	                    'created_at'	=> $date
	                )
	            );

		        Mail::send('email.accessUsers',['username' => $user->username, 'email' => $user->email, 'password' => $password], function($message) {
		            $message->to(Input::get('email'))
		                ->subject('Zugangsdaten für Ihren Account bei everdo');
		        });

	            return Redirect::to('users')
        			->with('successAddUser', 'Benutzer <strong>'.$input['username'].'</strong> erfolgreich angelegt.');

        }

	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$users = Users::find($id);
		// return $users;
		return View::make('resource.users.edit', compact('users'));
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = Input::all();
        $users = new Users();

		$validator = Validator::make( $input, Users::$rules['update'], Users::$messages['update']);

		if ( $validator->fails() ) {

	        return Redirect::to('users/'.$id.'/edit')
	        	->withErrors($validator)
	        	->withInput();		
		}

		else {

			DB::table('users')
                ->where('usersID', $id)
                ->update(
                    array(
                        'username'      => $input['username'],
                        'email'   		=> $input['email'],
                        'rolesID'       => $input['rolesID']
                )
            );

            return Redirect::to('users')
        		->with('successUpdateUser', 'Benutzer <strong>'.$input['username'].'</strong> erfolgreich aktualisiert.');
		}

		return $validator;
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$input = Input::all();

		DB::table('users')
			->where('usersID', '=', $id)
			->delete();

        return Redirect::to('users')
			->with('successDeleteUser', 'Benutzer <strong>'.$input['username'].'</strong> erfolgreich gelöscht.');

	}


}
