<?php

class AngCheckController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /angcheck
	 *
	 * @return Response
	 */
	public function index()
	{

	}

	/**
	 * Show the form for creating a new resource.
	 * GET /angcheck/create
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	private function saveArray($checklistID, $tasksArray, $parentID){

		$tempArrayTaskIDs = [];
		foreach ($tasksArray as $key => $value) {
	       	$task = Tasks::firstOrCreate(array('tasksID' => $value->tasksID, 'checklistsID' => $checklistID));
	       	$task->checklistsID = $checklistID;
	       	$task->tasksname = $value->tasksname;
	       	$task->parentID = $parentID;
	       	$task->order = $key;
	       	$task->tasksnote = $value->tasksnote;

	       	$task->save();
	       	array_push($tempArrayTaskIDs, $task->tasksID);

	       	//schaut ob Kindelemente vorhanden sind
	       	if (sizeof($value->nodes) > 0){
	       		$tempArrayChildTaskIDs = $this->saveArray($checklistID, $value->nodes, $task->tasksID);
	       		$tempArrayTaskIDs = array_merge($tempArrayTaskIDs , $tempArrayChildTaskIDs);

	       	}
       	}

       	return $tempArrayTaskIDs;
	}


	/**
	 * Store a newly created resource in storage.
	 * POST /angcheck
	 *
	 * @return Response
	 */
	public function store()
	{	
		// First we fetch the Request instance
		$request = Request::instance();

		// Now we can get the content from it
		$content = $request->getContent();

		// Raw-Post decode to JSON-File
		$jsonfile = json_decode($content);

		$date = new DateTime;

		//$checklist = Checklists::firstOrCreate(array('checklistsID' => $jsonfile[0]->checklistsID));
		//$checklist->checklistsname=$jsonfile[0]->checklistsname;
		//$checklist->updated_at = $date;

		//wenn Checklists vorhanden
		if (isset($jsonfile[0]->checklistsID)){
			//return $jsonfile[0]->checklistsname;
			$changeName = DB::table('checklists')
				->where('checklistsID', $jsonfile[0]->checklistsID)
				->update(array(
					'checklistsname' => $jsonfile[0]->checklistsname,
					'updated_at' 		=> $date
				));
			// hole checklist mit id aus db
			//ändere name
			//speichere in db
			$tempArrayTaskIDs = $this->saveArray($jsonfile[0]->checklistsID, $jsonfile, 0);
			//var_dump($tempArrayTaskIDs);

			//delete all tasks that are not in Json anymore
			DB::table('tasks')
				->where('checklistsID', $jsonfile[0]->checklistsID)
       			->whereNotIn('tasksID', $tempArrayTaskIDs)
       			->delete();
		}
		//wenn Checklist noch nicht vorhanden
		else{
			$usersID = Auth::id();
			$checklists = new Checklists();
			$checklists->usersID = $usersID;
			$checklists->checklistsname = $jsonfile[0]->checklistsname;
			$checklists->collectionsID = null;
			$checklists->available = '0';
		    $checklists->availableproperty = null;
		    $checklists->availablevalue= null;
			$checklists->note= null;
			$checklists->updated_at = '0000-00-00 00:00:00';
			$checklists->save();
			$this->saveArray($checklists->checklistsID, $jsonfile, 0);
		}

		return Response::json(array('success' => true));
	}


	private function getChildTasks($checklistID, $parentID){

		$tasks = DB::table('tasks')
			->where ('checklistsID', '=', $checklistID)
			->where ('parentID', '=', $parentID)
			->orderBy('order') //distinct
			->get();

		foreach ($tasks as $key => $value) {
			$value->editing = false;
			$value->checked = $value->checked;
			$value->note_editing = false;
			$value->id = $value->tasksID;
			$childTasks = $this->getChildTasks($checklistID, $value->tasksID);
			$value->nodes = $childTasks;
		}

		return $tasks;
	}
	

	/**
	 * Display the specified resource.
	 * GET /angcheck/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$allTasks = $this->getChildTasks($id, 0);
			return $allTasks;
		Response::json($allTasks);
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /angcheck/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$allTasks = $this->getChildTasks($id, 0);
		//return $allTasks;
		return Response::json($allTasks);
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /angcheck/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /angcheck/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}