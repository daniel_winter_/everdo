<?php

class LoginController extends \BaseController {

	public function showLogin(){
		return View::make('login');
	}

	public function showRegister(){
		return View::make('register');
	}

	public function forgetpassword(){
		return View::make('forgetpassword');
	}

	public function getPassword(){
		$input = Input::all();

		$validator = Validator::make( $input, Users::$rules['forgetpassword'], Users::$messages['forgetpassword'] );

		if ($validator->fails()){
			return Redirect::to('forgetpassword')
				->withErrors($validator)
				->withInput();
		}
		else {
	    		$date = new DateTime;
				$user = new User;
	    		$user->email = Input::get('email');
	    		$password = str_random(8);

				DB::table('users')
		            ->where('email', $user->email)
		            ->update(
		                array(
		                    'password'    	=> Hash::make($password),
		                    'updated_at'	=> $date
		            )
		        );

		        Mail::send('email.forgetPassword',['email' => $user->email, 'password' => $password], function($message) {
		            $message->to(Input::get('email'))
		                ->subject('Neues Passwort von everdo');
		        });

	            return Redirect::to('login')->with('successForgetPassword', 1);
        }
	}

	public function handleLogin()
	{
		$input = Input::all();

		$validator = Validator::make( $input, Users::$rules['login'], Users::$messages['login'] );

		if ($validator->fails()){
			return Redirect::to('login')
				->withErrors($validator)
				->withInput();
		}else{
			$date = new DateTime;
			//userdata for authentification
			$userdata = array(
				'username' 		=> Input::get('username'),
				'password' 		=> Input::get('password'),
				'confirmed' 	=> 1
			);

			if (Auth::attempt($userdata)) {
				//Timestamp dann hinzufuegen, Table User mit Timestamp updaten
				// validation successful!
				if (Auth::user()->rolesID == 2) {
					return Redirect::to('assignments');
				}
				else {
					return Redirect::to('dashboard');
				}
					
			}else{
				//when validation not successful, send back to login-form
				return Redirect::to('login')->withErrors(array('password' => 'Passwort falsch'))->withInput(Input::except('password'));
			}
		}

	}

	public function store(){
		$data = Input::only(['username','email','password','password_confirmation']);

		$validator = Validator::make( $data, Users::$rules['register'], Users::$messages['register'] );

		if ($validator->fails()){
			return Redirect::to('register')
				->withErrors($validator)
				->withInput();
		}else{

			$confirmation_code = str_random(30);

			$user = new User;
        	$user->username = Input::get('username');
    		$user->email = Input::get('email');
    		$user->password = Hash::make(Input::get('password'));

    		//create new entry in settings table
    		$settings = new Settings;
    		$settings->settingslogo = 'default.svg';
    		$settings->settingscolor = '#41AD78';
    		$settings->save();

    		$getSettingsID=$settings->settingsID;

    		$user->rolesID = '1';
    		$user->settingsID = $getSettingsID;
    		$user->confirmation_code = $confirmation_code;
    		$user->save();

	        Mail::send('email.verify',['confirmation_code' => $confirmation_code, 'username' => $user->username, 'email' => $user->email], function($message) {
	            $message->to(Input::get('email'), Input::get('username'))
	                ->subject('Bestätigung Ihres Accounts');
	        });

	        //Flash::message('Thanks for signing up! Please check your email.');

        	return Redirect::to('login')->with('activeAccountAlert', 1);
        }
    }

    public function confirm($confirmation_code){
        if(!$confirmation_code)
        {
            throw new InvalidConfirmationCodeException;
        }

        $user = User::whereConfirmationCode($confirmation_code)->first();

        if (!$user)
        {
            throw new InvalidConfirmationCodeException;
        }

        $user=DB::table('users')
            ->where('confirmation_code', $confirmation_code)
            ->update(array('confirmed' => 1,'confirmation_code' => null));

        //Flash::message('You have successfully verified your account.');

        return Redirect::to('login');
    }
    
   	public function logout(){
		if(Auth::check()){
  			Auth::logout();
		}
 		return Redirect::route('login');
	}

	/**
	 * Display a listing of the resource.
	 * GET /login
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /login/create
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Display the specified resource.
	 * GET /login/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /login/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /login/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /login/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}