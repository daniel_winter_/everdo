<?php

class AssignmentsuserschecklistsController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$usersID = Auth::id();
		$date = new DateTime;

		$assignments = DB::table('assignmentsuserschecklists as auc')
			->join('checklists as cl', 'auc.checklistsID', '=', 'cl.checklistsID')
			->select('auc.checklistsID', 'auc.checklistsstatusID', 'auc.duedate', 'auc.note','auc.sent', 'cl.checklistsname')
			->where('auc.checklistsstatusID', '!=', 3)
			// ->groupBy('cl.checklistsname')
			->where('auc.usersID', '=', $usersID)
			->where('auc.duedate', '>=', $date)
			->orderBy('auc.duedate')
			->get();

		return View::make('resource.auc.index', ['assignments' => $assignments]);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		
		$usersID = Auth::id();

		$input = Input::all();
		$reminderDate = $input['duedate'];
		$reminderTime = $input['reminder'];

		$combinedDateAndTime = $reminderDate . ' ' . $reminderTime;	
		$duedate = date("Y-m-d H:i:s", strtotime($combinedDateAndTime));

		$checklistsname = $input['checklistsname'];
		$userIDs = Input::get('userIDs');

		// return $input;

		if(empty($input['checklistsID'])) {
			$getAssignedChecklistID = DB::table('checklists')
				->select('checklistsID', 'usersID', 'checklistsname')
				->where('checklistsname', '=', $checklistsname)
				->where('usersID', '=', $usersID)
				->pluck('checklistsname');
			$checklistsID = $getAssignedChecklistID;
		}

		$auc = new Assignmentsuserschecklists();

		$validator = Validator::make( $input, Assignmentsuserschecklists::$rules['store'], Assignmentsuserschecklists::$messages['store'] );

		if ($validator->fails())
	    {
	            return Redirect::to('checklists/'.$checklistsID.'/edit')
	            	->withErrors($validator)
	            	->withInput();
	    }
	    else {

	    		$date = new DateTime;
	    		//foreach(array_keys($checklistsChecked) as $key) {
	    		foreach($userIDs as $uID) {

	    			$getAssignedUserEmail = DB::table('assignmentsuserschecklists as auc')
						->join('users as u', 'auc.usersID', '=', 'u.usersID')
						->select('u.email')
						->where('auc.usersID', '=', $uID)
						->pluck('u.email');

					$getAssignedUsername = DB::table('assignmentsuserschecklists as auc')
						->join('users as u', 'auc.usersID', '=', 'u.usersID')
						->select('u.username')
						->where('auc.usersID', '=', $uID)
						->pluck('u.username');

					// return $getAssignedUsername;

					$id = DB::table('assignmentsuserschecklists')->insertGetId(
		                array(
		                	'usersID'				=> $uID, //$input['usersID'],
		                	'checklistsID'			=> $checklistsID,
		                	'checklistsstatusID'	=> 1, //one means automatically open
		                	'note'					=> $input['note'] ,
		                	'duedate'				=> $duedate,
		                	'reminder'				=> $input['reminder'],
		                	'created_at'			=> $date
		                )
		            );
 
					$user = array(
						'email'=> $getAssignedUserEmail,
						'name'=> $getAssignedUsername
					);

					// the data that will be passed into the mail view blade template
					$data = array(
						'detail' => $checklistsname = $input['checklistsname'],
						'name'	=> $user['name']
					);

					Mail::send('email.infoAssignedChecklist', $data, function($message) use ($user)
					{
					  $message->from('everdo@design-winter.de', 'Ihr Everdo Team');
					  $message->to($user['email'], $user['name'])->subject('Neue Checkliste vorhanden');
					});
				}
				
	            return Redirect::to('checklists')->with('successAddAssignment', 1);
        }
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		return $id;
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$usersID = Auth::id();

		$assignments = DB::table('assignmentsuserschecklists as auc')
			->join('checklists as cl', 'auc.checklistsID', '=', 'cl.checklistsID')
			->select('auc.checklistsID', 'auc.checklistsstatusID', 'auc.duedate', 'auc.note', 'auc.noteFromUser', 'cl.checklistsname')
			->groupBy('cl.checklistsname')
			->where('auc.usersID', '=', $usersID)
			->where('cl.checklistsID', '=', $id)
			->get();

		// return $assignments;
		return View::make('resource.auc.edit', ['assignments' => $assignments]);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$usersID = Auth::id();

		$input = Input::all();
		$reminderDate = $input['duedate'];
		$reminderTime = $input['reminder'];

		$combinedDateAndTime = $reminderDate . ' ' . $reminderTime;	
		$duedate = date("Y-m-d H:i:s", strtotime($combinedDateAndTime));

		$checklistsID = $input['checklistsID'];
		$userIDs = Input::get('userIDs');

		$auc = new Assignmentsuserschecklists();

		$validator = Validator::make( $input, Assignmentsuserschecklists::$rules['store'], Assignmentsuserschecklists::$messages['store'] );

		if ($validator->fails())
	    {
	            return Redirect::to('checklists/'.$id.'/edit')
	            	->withErrors($validator)
	            	->withInput();
	    }
	    else {

	    		$date = new DateTime;
	    		//foreach(array_keys($checklistsChecked) as $key) {
	    		foreach($userIDs as $uID) {

	    			$getAssignedUserEmail = DB::table('assignmentsuserschecklists as auc')
						->join('users as u', 'auc.usersID', '=', 'u.usersID')
						->select('u.email')
						->where('auc.usersID', '=', $uID)
						->pluck('u.email');

					$getAssignedUsername = DB::table('assignmentsuserschecklists as auc')
						->join('users as u', 'auc.usersID', '=', 'u.usersID')
						->select('u.username')
						->where('auc.usersID', '=', $uID)
						->pluck('u.username');

					$id = DB::table('assignmentsuserschecklists')->insertGetId(
		                array(
		                	'usersID'				=> $uID, //$input['usersID'],
		                	'checklistsID'			=> $checklistsID,
		                	'checklistsstatusID'	=> 1, //one means automatically open
		                	'note'					=> $input['note'] ,
		                	'duedate'				=> $duedate,
		                	'reminder'				=> $input['reminder'],
		                	'created_at'			=> $date
		                )
		            );
 
					$user = array(
						'email'		=> $getAssignedUserEmail,
						'name'		=> $getAssignedUsername
					);

					// the data that will be passed into the mail view blade template
					$data = array( 
						'detail'	=> $checklistsname = $input['checklistsname'],
						'name'		=> $user['name']
					);

					Mail::send('email.infoAssignedChecklist', $data, function($message) use ($user)
					{
					  $message->from('everdo@design-winter.de', 'Ihr Everdo Team');
					  $message->to($user['email'], $user['name'])->subject('Neue Checkliste vorhanden');
					});
				}

				

	            return Redirect::to('checklists')->with('successAddAssignment', 1);
        }
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
