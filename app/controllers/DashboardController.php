<?php

class DashboardController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{

		$usersID = Auth::id();

		$settingsID = DB::table('users')->where('usersID', $usersID)->pluck('settingsID');

		$lastCreatedCl = DB::table('checklists as cl')
			->leftjoin('users as u', 'cl.usersID', '=', 'u.usersID')
			->select('cl.checklistsname', 'cl.checklistsID', 'u.username', 'cl.created_at')
			->where ('cl.usersID', '=', $usersID)
			->orderBy('cl.created_at', 'desc')
			->take(1)
			->get();
			// ->paginate(1);

		$lastChangedCl = DB::table('checklists as cl')
			->leftjoin('users as u', 'cl.usersID', '=', 'u.usersID')
			->select('cl.checklistsname', 'cl.checklistsID', 'u.username', 'cl.updated_at')
			->where ('cl.usersID', '=', $usersID)
			->orderBy('cl.updated_at', 'desc')
			->take(1)
			->get();
	
		$lastDoneCl = DB::table('assignmentsuserschecklists as auc')
			->join('checklists as cl', 'auc.checklistsID', '=', 'cl.checklistsID')
			->join('users as u', 'auc.usersID', '=', 'u.usersID')
			->select('auc.usersID', 'auc.checklistsID', 'auc.updated_at', 'auc.checklistsstatusID', 'cl.checklistsname', 'cl.checklistsID', 'u.username')
			->where('auc.usersID', '=', $usersID)
			->where('auc.checklistsstatusID', '=', 3)
			->orderBy('auc.updated_at', 'desc')
			->take(1)
			->get();
				
		// return $lastChangedCl;
		return View::make('resource.dashboard.index', ['lastCreatedCl' => $lastCreatedCl, 'lastChangedCl' => $lastChangedCl, 'lastDoneCl' => $lastDoneCl]);
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


}
