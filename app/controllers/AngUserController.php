<?php

class AngUserController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /anguser
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /anguser/create
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}



	private function saveArray($checklistID, $tasksArray, $parentID){

		//$tempArrayTaskIDs = [];
		foreach ($tasksArray as $key => $value) {
	       	$task = Tasks::firstOrCreate(array('tasksID' => $value->tasksID, 'checklistsID' => $checklistID));
	       	$task->checklistsID = $checklistID;
	       	$task->tasksname = $value->tasksname;
	       	$task->parentID = $parentID;
	       	$task->order = $key;
	       	$task->tasksnote = $value->tasksnote;
	       	$task->checked = $value->checked;

	       	$task->save();
	       	//array_push($tempArrayTaskIDs, $task->tasksID);

	       	//schaut ob Kindelemente vorhanden sind
	       	if (sizeof($value->nodes) > 0){
	       		$this->saveArray($checklistID, $value->nodes, $task->tasksID);
	       		//$tempArrayTaskIDs = array_merge($tempArrayTaskIDs , $tempArrayChildTaskIDs);

	       	}
       	}
	}
	/**
	 * Store a newly created resource in storage.
	 * POST /anguser
	 *
	 * @return Response
	 */
	public function store()
	{
		// First we fetch the Request instance
		$userID = Auth::id();
		$request = Request::instance();

		//return $userID;
		// Now we can get the content from it
		$content = $request->getContent();

		// Raw-Post decode to JSON-File
		$jsonfile = json_decode($content);
		//$date = new DateTime;

		//$checklist = Checklists::firstOrCreate(array('checklistsID' => $jsonfile[0]->checklistsID));
		//$checklist->checklistsname=$jsonfile[0]->checklistsname;
		//$checklist->updated_at = $date;

		//wenn Checklists vorhanden
		if (isset($jsonfile[0]->checklistsID)){
			//return $jsonfile[0]->checklistsname;
			$changeStatus = DB::table('assignmentsuserschecklists')
				->where('checklistsID', $jsonfile[0]->checklistsID)
				->where('usersID', $userID)
				->update(array('checklistsstatusID' => 2));
			// hole checklist mit id aus db
			//ändere name
			//speichere in db
			if(isset($jsonfile[0]->noteforchecklist)){
				$changeNote = DB::table('assignmentsuserschecklists')
					->where('checklistsID', $jsonfile[0]->checklistsID)
					->where('usersID', $userID)
					->update(array('noteFromUser' => $jsonfile[0]->noteforchecklist));
			}
			$this->saveArray($jsonfile[0]->checklistsID, $jsonfile, 0);
			$this->checkSentStatus();
			//var_dump($tempArrayTaskIDs)
		}
	}
	public function checkSentStatus(){
		$usersID = Auth::id();
		$getChecklistsToUsers = DB::table('assignmentsuserschecklists as auc')
			->join('users as u', 'auc.usersID', '=', 'u.usersID')
			->select('auc.checklistsID', 'auc.usersID')
			->where('auc.usersID', '=', $usersID)
			->groupBy('auc.checklistsID')
			->get();

		$sortChecklist = array_map(function ($object) { return $object->checklistsID; }, $getChecklistsToUsers);
		//return $sortChecklist;
		
		//return $check_count;

		$helper = 0;
		$helper = array();
		$count_that=0;

		//Hilfe fuer die Ausgabe ob die Checkliste gesendet werden kann oder nicht
		$helper2 = array();
		$count_that2=0;
		foreach ($sortChecklist as $key) {
			$getChecklist = DB::table('tasks')
				->leftjoin('checklists', 'tasks.checklistsID', '=', 'checklists.checklistsID')
				->select('checklists.checklistsID', 'tasks.tasksID', 'tasks.checked')
				->where ('checklists.checklistsID', '=', $key)
				//->whereNotNull('checklists.collectionsID')
				//->groupBy('checklists.checklistsname')
			    ->get();
			$helper[$count_that]=$getChecklist;
			$count_that++;
		}

		$hasCheckedNot=false;
		$tempCheckID = 0;
		foreach ($helper as $key2) {
			foreach ($key2 as $keyZ) {
				if($keyZ->checked == 1){
					$hasCheckedNot = true;
					$tempCheckID = $keyZ->checklistsID;
				}else{
					$tempCheckID = $keyZ->checklistsID;
					$hasCheckedNot=false;
					break;
				}
			}
			if($hasCheckedNot) {
				DB::table('assignmentsuserschecklists')
	                ->where('checklistsID', $tempCheckID)
	                ->update(
	                    array(
	                        'sent'=> 1,
	                )
	            );
				$helper2[$count_that2]=$tempCheckID."isrichtig";
			}else{
				DB::table('assignmentsuserschecklists')
	                ->where('checklistsID', $tempCheckID)
	                ->update(
	                    array(
	                        'sent'=> 0,
	                )
	            );
				$helper2[$count_that2]=$tempCheckID."isfalsch";
			}
			$count_that2++;
		}
	}

	/**
	 * Display the specified resource.
	 * GET /anguser/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /anguser/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /anguser/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /anguser/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}