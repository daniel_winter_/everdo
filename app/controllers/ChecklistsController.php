<?php

class ChecklistsController extends BaseController {

	public function getOpenChecklists() {
		$usersID = Auth::id();
		$openUserChecklists = DB::table('checklists as cl')
			->leftjoin('assignmentsuserschecklists as auc', 'cl.checklistsID', '=', 'auc.checklistsID')
			->leftjoin('users as u', 'auc.usersID', '=', 'u.usersID')
			->select('cl.checklistsID', 'cl.checklistsname', 'auc.checklistsstatusID', 'auc.duedate', 'auc.reminder', 'u.username')
			->where('cl.usersID', $usersID)
			->where('auc.checklistsstatusID', '=', 1)
			->get();

		return $openUserChecklists;
	}

	public function getInProgressChecklists() {
		$usersID = Auth::id();

		$inProgressUserChecklists = DB::table('checklists as cl')
			->leftjoin('assignmentsuserschecklists as auc', 'cl.checklistsID', '=', 'auc.checklistsID')
			->leftjoin('users as u', 'auc.usersID', '=', 'u.usersID')
			->select('cl.checklistsID', 'cl.checklistsname', 'auc.checklistsstatusID', 'auc.duedate', 'auc.reminder', 'u.username')
			->where('cl.usersID', $usersID)
			->where('auc.checklistsstatusID', '=', 2)
			->get();

		return $inProgressUserChecklists;
	}

	public function getClosedChecklists() {
		$usersID = Auth::id();

		$closedUserChecklists = DB::table('checklists as cl')
			->leftjoin('assignmentsuserschecklists as auc', 'cl.checklistsID', '=', 'auc.checklistsID')
			->leftjoin('users as u', 'auc.usersID', '=', 'u.usersID')
			->select('cl.checklistsID', 'cl.checklistsname', 'auc.checklistsstatusID', 'auc.duedate', 'auc.reminder', 'auc.noteFromUser', 'u.username')
			->where('cl.usersID', $usersID)
			->where('auc.checklistsstatusID', '=', 3)
			->get();

		return $closedUserChecklists;
	}

	public function openChecklists() {
		$openUserChecklists = $this->getOpenChecklists();
		return View::make('resource.checklists.showOpenChecklists', ['openUserChecklists' => $openUserChecklists]);
	}

	public function inProgressChecklists() {
		$inProgressUserChecklists = $this->getInProgressChecklists();
		return View::make('resource.checklists.showInprogressChecklists', ['inProgressUserChecklists' => $inProgressUserChecklists]);
	}

	public function closedChecklists() {
		$closedUserChecklists = $this->getClosedChecklists();
		return View::make('resource.checklists.showClosedChecklists', ['closedUserChecklists' => $closedUserChecklists]);
	}

	public function allChecklists() {
		$openUserChecklists = $this->getOpenChecklists();
		$inProgressUserChecklists = $this->getInProgressChecklists();
		$closedUserChecklists = $this->getClosedChecklists();

		return View::make('resource.checklists.showAllChecklists', ['openUserChecklists' => $openUserChecklists, 'inProgressUserChecklists' => $inProgressUserChecklists, 'closedUserChecklists' => $closedUserChecklists]);
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$usersID = Auth::id();
		
		$coll_count = DB::table('checklists')
			->leftjoin('collections', 'checklists.collectionsID', '=', 'collections.collectionsID')
			->select('checklists.checklistsID', 'checklists.collectionsID', 'checklists.checklistsname', 'collections.collectionsname')
			->where ('checklists.usersID', '=', $usersID)
			->whereNotNull('checklists.collectionsID')
			->groupBy('checklists.collectionsID')
		    ->get();

		$getColValues = DB::table('checklists')
			->leftjoin('collections', 'checklists.collectionsID', '=', 'collections.collectionsID')
			->select('checklists.collectionsID', 'collections.collectionsname')
			->where ('checklists.usersID', '=', $usersID)
			->whereNotNull('checklists.collectionsID')
			->groupBy('checklists.collectionsID')
		    ->get();

		$check_count = DB::table('checklists')
			->leftjoin('collections', 'checklists.collectionsID', '=', 'collections.collectionsID')
			->select('checklists.checklistsID', 'checklists.collectionsID', 'checklists.checklistsname', 'collections.collectionsname')
			->where ('checklists.usersID', '=', $usersID)
			// ->whereNull('checklists.collectionsID')
			->groupBy('checklists.checklistsname')
		    ->get();

		$sort_coll_count = array_map(function ($object) { return $object->collectionsID; }, $coll_count);

		$sort_check_count = array_map(function ($object) { return $object->checklistsID; }, $check_count);

		//return $check_count;

		//COLLECTIONS 
		$helper = array();
		$count_that=0;
		foreach ($sort_coll_count as $key) {
			$getChecklist = DB::table('checklists')
				->leftjoin('collections', 'checklists.collectionsID', '=', 'collections.collectionsID')
				->select('checklists.collectionsID', 'checklists.checklistsname', 'checklists.checklistsID', 'collections.collectionsID', 'collections.collectionsname')
				->where ('collections.collectionsID', '=', $key)
				//->whereNotNull('checklists.collectionsID')
				//->groupBy('checklists.checklistsname')
			    ->get();
			$helper[$count_that]=$getChecklist;
			$count_that++;
		}

		//CHECKLISTS 
		$helper2 = array();
		$count_that2=0;
		foreach ($sort_check_count as $key2) {
			$getChecklist2 = DB::table('checklists')
				->leftjoin('collections', 'checklists.collectionsID', '=', 'collections.collectionsID')
				->select('checklists.checklistsname', 'checklists.checklistsID')
				->where ('checklists.checklistsID', '=', $key2)
				//->whereNotNull('checklists.collectionsID')
				->groupBy('checklists.checklistsname')
			    ->get();
			$helper2[$count_that2]=$getChecklist2;
			$count_that2++;
		}

		// return $helper2;
		return View::make('resource.checklists.index')->with(array('collections' => $helper, 'getColValues'=>$getColValues, 'checklists'=>$helper2));
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{

		$usersID = Auth::id();
		$settingsID = DB::table('users')->where('usersID', $usersID)->pluck('settingsID');

		$getAllUsers = DB::table('users')
			->select('usersID', 'settingsID', 'username')
			->where('settingsID', '=', $settingsID)
			->where('rolesID', '=', 2) //nur User keine Admins
			->whereNotIn('usersID', array($usersID))
			->lists('username' , 'usersID');

		$getAllCollections = DB::table('checklists as cl')
			->leftjoin('collections as co', 'cl.collectionsID', '=', 'co.collectionsID')
			->select('co.collectionsID', 'co.collectionsname')
			->where('usersID', $usersID)
			->whereNotNull('co.collectionsID')
			->lists('collectionsname' , 'collectionsID');

		$getAllChecklists = DB::table('checklists')
			->select('checklistsID', 'checklistsname')
			->where('usersID', $usersID)
			->groupBy('checklistsname')
			->lists('checklistsname' , 'checklistsID');

		$getMaxNumberOfUsers = DB::table('users as u')
			->whereNotIn('usersID', array($usersID))
			->where('u.settingsID', '=', $settingsID)
			->count();
			
		return View::make('resource.checklists.create', ['getAllChecklists' => $getAllChecklists, 'getAllCollections' => $getAllCollections, 'getAllUsers' => $getAllUsers, 'getMaxNumberOfUsers' => $getMaxNumberOfUsers]);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$usersID = Auth::id();
		$input = Input::all();
		$availableChecked = Input::get('available');

		// return $input;

		$checklists = new Checklists();

		$validator = Validator::make( $input, Checklists::$rules['create'], Checklists::$messages['create'] );

		if ($validator->fails())
	    {
	            return Redirect::to('checklists/create')
	            	->withErrors($validator)
	            	->withInput();
	    }
	    else {
	    		$colID = $input['collectionsID'];
	    		if ($input['collectionsID'] == null) {
	    			$colID = null;
	    		}
	    		else {
	    			$colID = $input['collectionsID'];
	    		}
	    		$date = new DateTime;
				$id = DB::table('checklists')->insertGetId(
	                array(
	                	'usersID'			=> $usersID,
	                	'collectionsID'		=> $colID,
	                	'checklistsname'	=> $input['checklistsname'],
	                	'available'			=> $availableChecked,
	                	'availableproperty'	=> null,
	                	'availablevalue'	=> null,
	                	'note'				=> $input['note'],
	                	'created_at' 		=> $date,
						// 'updated_at' 		=> $date
	                )
	            );

	            return Redirect::to('checklists')->with('successAddChecklist', 1);
        }
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		// return "test"; 
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$usersID = Auth::id();
		$settingsID = DB::table('users')->where('usersID', $usersID)->pluck('settingsID');

		$getAllUsers = DB::table('users')
			->select('usersID', 'settingsID', 'username', 'rolesID')
			->where('settingsID', '=', $settingsID)
			->where('rolesID', '=', 2) //nur User keine Admins
			->whereNotIn('usersID', array($usersID))
			->lists('username' , 'usersID');

		$checklists = DB::table('checklists as cl')
			->select('cl.checklistsID', 'cl.usersID', 'cl.checklistsname')
			->where ('cl.usersID', '=', $usersID)
			->where('cl.checklistsID', '=', $id)
		    ->get();

		$assignedUsersToChecklist = DB::table('assignmentsuserschecklists as auc')
			->leftjoin('users as u', 'auc.usersID', '=', 'u.usersID')
			->select('u.username', 'auc.usersID', 'auc.checklistsID')
		    ->get();

		$getAllCollections = DB::table('checklists as cl')
			->leftjoin('collections as co', 'cl.collectionsID', '=', 'co.collectionsID')
			->select('co.collectionsID', 'co.collectionsname')
			->where('usersID', $usersID)
			->whereNotNull('co.collectionsID')
			->lists('collectionsname' , 'collectionsID');


		// return $getAllUsers;
		return View::make('resource.checklists.edit', ['getAllUsers' => $getAllUsers, 'checklists' => $checklists, 'assignedUsersToChecklist' => $assignedUsersToChecklist, 'getAllCollections' => $getAllCollections]);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = Input::all();

		// return $input;
        $id = $input['usersID'];

        $checklists = new Checklists();

		$validator = Validator::make($input, Checklists::$rules['update'], Checklists::$messages['update']);

		if ( $validator->fails() ) {

	        return Redirect::to('checklists/'.$id.'/edit')
	        	->withErrors($validator)
	        	->withInput();		
		}

		else {
			$date = new DateTime;
			DB::table('checklists')
				->where('checklistsID', $input['checklistsID'])
                ->where('usersID', $id)
                ->update(
                    array(
                    	// 'usersID'		=> $input['usersID']
                        'updated_at'    => $date
                )
            );

            return Redirect::to('checklists')->with('successUpdateChecklists', 1);
			
		}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$input = Input::all();

		DB::table('checklists')
			->where('checklistsname', '=', $input['checklistsname'])
			// ->where('checklistsID', '=', $id)
			->delete();

        return Redirect::to('checklists')
        	->with('successDeleteChecklists', 'Checkliste <strong>'.$input['checklistsname'].'</strong> erfolgreich gelöscht.');
	}


}
