<?php

class SettingsController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return View::make('resource.settings.index');
	}

	public function showHelp()
	{
		return View::make('resource.settings.help');
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$usersID = Auth::id();
		$settingsID = DB::table('users')->where('usersID', $usersID)->pluck('settingsID');

		$input = Input::all();

		// return $input;

		$settings = new Settings();

		if(empty($input['image'])){
			return Redirect::to('settings')
		    	->withErrors("Sie müssen ein Logo hochladen.");  
        }
        else {
        	$filename = Input::file('image')->getClientOriginalName();
        }

		

		$validator = Validator::make( $input, Settings::$rules['store'], Settings::$messages['store'] );

		if($validator->fails()){
            return Redirect::to('settings')
	            	->withErrors($validator)
	            	->withInput();
        }

        else {
			
			DB::table('settings')
				->where('settingsID', $settingsID)
				->update(array(
					'settingslogo' => $filename,
					'settingscolor' => $input['color'])
			);

 			Input::file('image')->move('../public/assets/images/logo/',Input::file('image')->getClientOriginalName());

			return Redirect::to('settings')->with('successAddSettingsLogo', 1);
        }
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
