<?php

class BaseController extends Controller {

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}

	public function __construct() {

		$usersID = Auth::id();
		$settingsID = DB::table('users')->where('usersID', $usersID)->pluck('settingsID');
  
		$settingsLogo = DB::table('settings')
			->select('settingslogo')
			->where('settingsID', '=', $settingsID)
			->pluck('settingslogo');

		$getUserSettingsLogo = url('/assets/images/logo/'.$settingsLogo);

		$settingsColor = DB::table('settings')
			->select('settingscolor')
			->where('settingsID', '=', $settingsID)
			->pluck('settingscolor');

		//Alle Entwürfe
		// $allChecklists = DB::table('checklists')
		// 	->where('usersID', '=', $usersID)
		// 	->count(DB::raw('DISTINCT checklistsname'));

		$allChecklists = DB::table('assignmentsuserschecklists as auc')
			->join('checklists as cl', 'auc.checklistsID', '=', 'cl.checklistsID')
			->select('auc.checklistsID')
			->where('cl.usersID', '=', $usersID)
			->count();


		$openChecklists = DB::table('assignmentsuserschecklists as auc')
			->join('checklists as cl', 'auc.checklistsID', '=', 'cl.checklistsID')
			->select('cl.usersID')
			->where('cl.usersID', '=', $usersID)
			->where('auc.checklistsstatusID', '=', 1)
			->count();

		$inProgressChecklists = DB::table('assignmentsuserschecklists as auc')
			->join('checklists as cl', 'auc.checklistsID', '=', 'cl.checklistsID')
			->select('cl.usersID')
			->where('cl.usersID', '=', $usersID)
			->where('auc.checklistsstatusID', '=', 2)
			->count();

		$closedChecklists = DB::table('assignmentsuserschecklists as auc')
			->join('checklists as cl', 'auc.checklistsID', '=', 'cl.checklistsID')
			->select('cl.usersID')
			->where('cl.usersID', '=', $usersID)
			->where('auc.checklistsstatusID', '=', 3)
			->count();

		$getNumberUsers = DB::table('users as u')
			->whereNotIn('usersID', array($usersID))
			->where('u.settingsID', '=', $settingsID)
			->count();

		$getNumberUsersWithoutAdmins = DB::table('users')
			->whereNotIn('usersID', array($usersID))
			->where('rolesID', '=', 2)
			->where('settingsID', '=', $settingsID)
			->count();

		$lastCreatedUser = DB::table('users')
			->select('usersID', 'username', 'created_at')
			->where('settingsID', $settingsID)
			->whereNotIn('usersID', array($usersID))
			->orderBy('created_at', 'desc')
			->take(1)
			->get();

	  	View::share('getUserSettingsLogo', $getUserSettingsLogo);
	  	View::share('allChecklists', $allChecklists);
	  	View::share('openChecklists', $openChecklists);
	  	View::share('inProgressChecklists', $inProgressChecklists);
	  	View::share('closedChecklists', $closedChecklists);
	  	View::share('getNumberUsers', $getNumberUsers);
	  	View::share('getNumberUsersWithoutAdmins', $getNumberUsersWithoutAdmins);
	  	View::share('lastCreatedUser', $lastCreatedUser);

	 }




}
